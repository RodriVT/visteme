$(document).ready(function(){
    var close = 0;
    $("html").click(function() {
        var active = $('.sidebar-movil').data('active');
        if(active == 'true'){
            if(close > 0){
                console.log('cerrar');
                close = 0;
                $('#sidebar_btn').click();
                $('.sidebar-movil').data('active','false');
            }else{
                close++;
            }
        }
    });

    $("#sidebar_btn").click(function(e) {
        e.stopPropagation();
        var active = $('.sidebar-movil').data('active');
        if(active == 'true'){
            close = 0;
            $('.sidebar-movil').data('active','false');
        }else{
            $('.sidebar-movil').data('active','true');
        }
    });

    $('.sidebar-movil').click(function (e) {
        e.stopPropagation();
    });
});
function alertMessage(mensaje, type){
    if(type == "error"){
        swal({
            title: "\u00A1Alerta!",
            text: mensaje,
            type: "error"
        });
    }
}
function logout(){
    $.post("login/logout",{}, function(data){
        if(data == '200'){
            window.location = "login";
        }
    });
}
function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
function validarExtension(datos) {
    var extensionesValidas = ".png, .gif, .jpeg, .jpg, .bmp, .tiff, .tif, .jfif";
    var ruta = datos['name'];
    var extension = ruta.substring(ruta.lastIndexOf('.') + 1).toLowerCase();
    var extensionValida = extensionesValidas.indexOf(extension);

    if(extensionValida < 0) {
        return false;
    } else {
        return true;
    }
}