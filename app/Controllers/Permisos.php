<?php

namespace App\Controllers;

class Permisos extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $perfiles = $this->perfilesModel->where('id_estatus',1)->findAll();

        $perfiles_data = array();
        foreach($perfiles as $perfil){
            $acciones = array();
            $acciones[] = array(
                'href'  => base_url().'/permisos/asignacion/'.$perfil['id_perfil'],
                'class' => 'btn-warning',
                'icon'  => 'fas fa-plus',
                'title' => 'Asignar Permisos'
            );
            $perfiles_data[] = array(
                'id_perfil'          => $perfil['id_perfil'],
                'perfil_descripcion'          => $perfil['perfil_descripcion'],
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista Permisos de Perfiles',
            'perfiles_data' => $perfiles_data,
        ];

        echo view('vw-header',$data);
        echo view('permisos/vw-permisos',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        foreach($_POST as $id_menu => $val)
        {
            $permiso = $this->permisosModel->where('id_perfil',$this->request->getPost('id_perfil'))->where('id_menu',$id_menu)->first();
            if($permiso == null){
                if(is_numeric($id_menu)){
                    $this->permisosModel->insert([
                        'id_perfil' => $this->request->getPost('id_perfil'),
                        'id_menu' => $id_menu,
                        'id_estatus' => 1,
                        'creator_user_id' => $session['id_usuario_session'],
                    ]);
                }
            }else{
                if(($permiso['id_estatus']) == 2){
                    $this->permisosModel->where('id_permiso_perfil', $permiso['id_permiso_perfil'])->set([
                        'id_estatus' => 1,
                        'updater_user_id' => $session['id_usuario_session'],
                    ])->update();
                }
            }
        }
        $permisos = $this->permisosModel->where('id_perfil',$this->request->getPost('id_perfil'))->findAll();
        foreach ($permisos as $permiso){
            if(!isset($_POST[$permiso['id_menu']])){
                $this->permisosModel->where('id_permiso_perfil', $permiso['id_permiso_perfil'])->set([
                    'id_estatus' => 2,
                    'updater_user_id' => $session['id_usuario_session'],
                ])->update();
            }
        }
        return redirect()->to(base_url('permisos'));
    }

    public function asignacion($id)
    {
        $session = $this->validaSesion();
        $opciones_parents = $this->opcionesMenuModel->where('parent_menu_id',0)->where('id_estatus',1)->orderBy('orden', 'ASC')->findAll();

        $row_perfil = $this->perfilesModel->where('id_perfil',$id)->first();
        $perfil = array(
            'id_perfil' => $row_perfil['id_perfil'],
            'perfil_descripcion' => $row_perfil['perfil_descripcion'],
        );
        $permisos = $this->permisosModel->where('id_perfil',$id)->where('id_estatus',1)->findAll();

        $opciones_menu = null;
        foreach ($opciones_parents as $opcion_parent) {
            $checked = false;
            foreach ($permisos as $permiso){
                if($opcion_parent['id_menu'] == $permiso['id_menu']){
                    $checked = true;
                }
            }
            $opciones_children = $this->opcionesMenuModel->where('parent_menu_id',$opcion_parent['id_menu'])->where('id_estatus',1)->orderBy('orden', 'ASC')->findAll();
            $opcionesChildren = null;
            foreach ($opciones_children as $opcion_children){
                $checked_son = false;
                foreach ($permisos as $permiso){
                    if($opcion_children['id_menu'] == $permiso['id_menu']){
                        $checked_son = true;
                    }
                }
                $opcionesChildren[] = array(
                    'parent_menu_id' => $opcion_parent['id_menu'],
                    'id_menu' => $opcion_children['id_menu'],
                    'etiqueta' => $opcion_children['etiqueta'],
                    'checked' => ($checked_son == true) ? 'checked' : '',
                );
            }
            $opciones_menu[] = array(
                'id_menu' => $opcion_parent['id_menu'],
                'etiqueta' => $opcion_parent['etiqueta'],
                'id_estatus' => $opcion_parent['id_estatus'],
                'fa_icon' => $opcion_parent['fa_icon'],
                'checked' => ($checked == true) ? 'checked' : '',
                'opciones_children' => $opcionesChildren,
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Asignación de Permisos',
            'button' => 'Asignar',
            'action' => base_url().'/permisos/create_action',
            'perfil' => $perfil,
            'opciones_menu' => $opciones_menu,
            'opciones_children' => $opcionesChildren,

        ];

        echo view('vw-header',$data);
        echo view('permisos/fm-permisos',$data);
        echo view('vw-footer',$data);
    }
}
