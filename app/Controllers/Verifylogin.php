<?php

namespace App\Controllers;

class Verifylogin extends BaseController
{
    public function login()
    {
        $session = \Config\Services::session($config);

        //$opciones = ['cost' => 10];
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');
        $user = $this->usuariosModel->where('username',$username)->first();
        if($user['intentos'] < 3){
            if($user != ""){
                if(password_verify($password, $user['password'])){
                    $this->usuariosModel->where('id_usuario', $user['id_usuario'] )->set(['updater_user_id' => 1, 'intentos' => 0])->update();
                    $sess_array = array(
                        'id_usuario' => $user['id_usuario'],
                        'username' => $user['username'],
                        'nombre' => $user['nombre'],
                        'ape_paterno' => $user['ape_paterno'],
                        'ape_materno' => $user['ape_materno'],
                        'foto_perfil' => $user['foto_perfil'],
                        'id_perfil' => $user['id_perfil'],
                    );
                    $session->set('logged_in',$sess_array);
                    return '200';
                }else{
                    if($user['intentos'] == 3){
                        return '401';
                    }else{
                        $intentos = $user['intentos'] + 1;
                        $this->usuariosModel->where('id_usuario', $user['id_usuario'] )->set(['updater_user_id' => 1, 'intentos' => $intentos])->update();
                        return '400';
                    }
                }
            }else{
                return '400';
            }
        }else{
            return '401';
        }
    }
}