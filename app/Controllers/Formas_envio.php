<?php namespace App\Controllers;

class Formas_envio extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $formas_envio = $this->formasEnvioModel->findAll();

        $formas_envio_data = array();
        foreach($formas_envio as $forma_envio){
            $acciones = array();
            switch ($forma_envio['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/formas_envio/edit/'.$forma_envio['id_forma_envio'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/formas_envio/baja/'.$forma_envio['id_forma_envio'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/formas_envio/alta/'.$forma_envio['id_forma_envio'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $formas_envio_data[] = array(
                'id_forma_envio'          => $forma_envio['id_forma_envio'],
                'forma_envio_descripcion'          => $forma_envio['forma_envio_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Formas de Envío',
            'action' => 'formas_envio/create',
            'formas_envio_data' => $formas_envio_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_formas_envio/vw-formas-envio',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Forma Envío',
            'button' => 'Crear',
            'action' => base_url().'/formas_envio/create_action',
    	    'id_forma_envio' => '',
    	    'forma_envio_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_formas_envio/fm-formas-envio',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $id = $this->formasEnvioModel->insert([
            'forma_envio_descripcion' => $this->request->getPost('forma_envio_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('formas_envio'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();
        $forma_envio = $this->formasEnvioModel->where('id_forma_envio',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Forma Envío',
            'button' => 'Actualizar',
            'action' => base_url().'/formas_envio/edit_action',
    	    'id_forma_envio' => $forma_envio['id_forma_envio'],
    	    'forma_envio_descripcion' => $forma_envio['forma_envio_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_formas_envio/fm-formas-envio',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_forma_envio');
        $this->formasEnvioModel->where('id_forma_envio', $id)->set([
            'forma_envio_descripcion' => $this->request->getPost('forma_envio_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('formas_envio'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $forma_envio = $this->formasEnvioModel->where('id_forma_envio',$id)->first();
        if ($forma_envio) {
            $this->formasEnvioModel->where('id_forma_envio', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('formas_envio'));
        }else{
            return redirect()->to(base_url('formas_envio'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $forma_envio = $this->formasEnvioModel->where('id_forma_envio',$id)->first();
        if ($forma_envio) {
            $this->formasEnvioModel->where('id_forma_envio', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('formas_envio'));
        }else{
            return redirect()->to(base_url('formas_envio'));
        }
    }
}