<?php namespace App\Controllers;

class Marcas extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $marcas = $this->marcasModel->findAll();

        $marcas_data = array();
        foreach($marcas as $marca){
            $acciones = array();
            switch ($marca['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/marcas/edit/'.$marca['id_marca'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/marcas/baja/'.$marca['id_marca'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/marcas/alta/'.$marca['id_marca'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $marcas_data[] = array(
                'id_marca'          => $marca['id_marca'],
                'marca_descripcion'          => $marca['marca_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Marcas',
            'action' => 'marcas/create',
            'marcas_data' => $marcas_data
        ];

        echo view('vw-header',$data);
        echo view('cat_marcas/vw-marcas',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Marca',
            'button' => 'Crear',
            'action' => base_url().'/marcas/create_action',
    	    'id_marca' => '',
    	    'marca_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_marcas/fm-marcas',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $id = $this->marcasModel->insert([
            'marca_descripcion' => $this->request->getPost('marca_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('marcas'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();
        $marca = $this->marcasModel->where('id_marca',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Marca',
            'button' => 'Actualizar',
            'action' => base_url().'/marcas/edit_action',
    	    'id_marca' => $marca['id_marca'],
    	    'marca_descripcion' => $marca['marca_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_marcas/fm-marcas',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_marca');
        $this->marcasModel->where('id_marca', $id)->set([
            'marca_descripcion' => $this->request->getPost('marca_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('marcas'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $marca = $this->marcasModel->where('id_marca',$id)->first();
        if ($marca) {
            $this->marcasModel->where('id_marca', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('marcas'));
        }else{
            return redirect()->to(base_url('marcas'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $marca= $this->marcasModel->where('id_marca',$id)->first();
        if ($marca) {
            $this->marcasModel->where('id_marca', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('marcas'));
        }else{
            return redirect()->to(base_url('marcas'));
        }
    }
}