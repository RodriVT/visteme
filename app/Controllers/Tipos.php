<?php namespace App\Controllers;

class Tipos extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $tipos = $this->tiposModel->findAll();

        $tipos_data = array();
        foreach($tipos as $tipo){
            $acciones = array();
            switch ($tipo['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/tipos/edit/'.$tipo['id_tipo'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/tipos/baja/'.$tipo['id_tipo'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/tipos/alta/'.$tipo['id_tipo'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $tipos_data[] = array(
                'id_tipo'          => $tipo['id_tipo'],
                'tipo_descripcion'          => $tipo['tipo_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Tipos',
            'action' => 'tipos/create',
            'tipos_data' => $tipos_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_tipos/vw-tipos',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Tipo',
            'button' => 'Crear',
            'action' => base_url().'/tipos/create_action',
    	    'id_tipo' => '',
    	    'tipo_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_tipos/fm-tipos',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $this->tiposModel->insert([
            'tipo_descripcion' => $this->request->getPost('tipo_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('tipos'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();
        $tipo = $this->tiposModel->where('id_tipo',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Tipo',
            'button' => 'Actualizar',
            'action' => base_url().'/tipos/edit_action',
    	    'id_tipo' => $tipo['id_tipo'],
    	    'tipo_descripcion' => $tipo['tipo_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_tipos/fm-tipos',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_tipo');
        $this->tamaniosModel->where('id_tipo', $id)->set([
            'tipo_descripcion' => $this->request->getPost('tipo_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('tipos'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $tipo = $this->tiposModel->where('id_tipo',$id)->first();
        if ($tipo) {
            $this->tiposModel->where('id_tipo', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('tipos'));
        }else{
            return redirect()->to(base_url('tipos'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $tipo = $this->tiposModel->where('id_tipo',$id)->first();
        if ($tipo) {
            $this->tiposModel->where('id_tipo', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('tipos'));
        }else{
            return redirect()->to(base_url('tipos'));
        }
    }
}