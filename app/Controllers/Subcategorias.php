<?php namespace App\Controllers;

class Subcategorias extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $subcategorias = $this->subcategoriasModel->findAll();

        $subcategorias_data = array();
        foreach($subcategorias as $subcategoria){
            $acciones = array();
            switch ($subcategoria['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/subcategorias/edit/'.$subcategoria['id_subcategoria'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/subcategorias/baja/'.$subcategoria['id_subcategoria'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/subcategorias/alta/'.$subcategoria['id_subcategoria'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $categoria = $this->categoriasModel->where('id_categoria',$subcategoria['id_categoria'])->first();
            $subcategorias_data[] = array(
                'id_subcategoria'          => $subcategoria['id_subcategoria'],
                'subcategoria_descripcion'          => $subcategoria['subcategoria_descripcion'],
                'categoria_descripcion'          => $categoria['categoria_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Subcategorías',
            'action' => 'subcategorias/create',
            'subcategorias_data' => $subcategorias_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_subcategorias/vw-subcategorias',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $categorias = $this->categoriasModel->where('id_estatus',1)->find();
        $data = [
            'session' => $session,
            'title' => 'Crear Subcategoría',
            'button' => 'Crear',
            'action' => base_url().'/subcategorias/create_action',
    	    'id_subcategoria' => '',
            'id_categoria' => '',
    	    'subcategoria_descripcion' => '',
            'categorias' => $categorias
        ];

        echo view('vw-header',$data);
        echo view('cat_subcategorias/fm-subcategorias',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $id = $this->subcategoriasModel->insert([
            'id_categoria' => $this->request->getPost('id_categoria'),
            'subcategoria_descripcion' => $this->request->getPost('subcategoria_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('subcategorias'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();
        $subcategoria = $this->subcategoriasModel->where('id_subcategoria',$id)->first();
        $categorias = $this->categoriasModel->where('id_estatus',1)->find();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Subcategoría',
            'button' => 'Actualizar',
            'action' => base_url().'/subcategorias/edit_action',
    	    'id_subcategoria' => $subcategoria['id_subcategoria'],
            'id_categoria' => $subcategoria['id_categoria'],
    	    'subcategoria_descripcion' => $subcategoria['subcategoria_descripcion'],
            'categorias' => $categorias
        ];

        echo view('vw-header',$data);
        echo view('cat_subcategorias/fm-subcategorias',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_subcategoria');
        $this->subcategoriasModel->where('id_subcategoria', $id)->set([
            'id_categoria' => $this->request->getPost('id_categoria'),
            'subcategoria_descripcion' => $this->request->getPost('subcategoria_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('subcategorias'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $subcategoria = $this->subcategoriasModel->where('id_subcategoria',$id)->first();
        if ($subcategoria) {
            $this->subcategoriasModel->where('id_subcategoria', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('subcategorias'));
        }else{
            return redirect()->to(base_url('subcategorias'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $subcategoria = $this->subcategoriasModel->where('id_subcategoria',$id)->first();
        if ($subcategoria) {
            $this->subcategoriasModel->where('id_subcategoria', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('subcategorias'));
        }else{
            return redirect()->to(base_url('subcategorias'));
        }
    }
}