<?php 
namespace App\Controllers;


class Colores extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();

        $colores = $this->coloresModel->findAll();

        $colores_data = array();
        foreach($colores as $color){
            $acciones = array();
            switch ($color['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/colores/edit/'.$color['id_color'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/colores/baja/'.$color['id_color'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/colores/alta/'.$color['id_color'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $colores_data[] = array(
                'id_color'          => $color['id_color'],
                'color_descripcion'          => $color['color_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Colores',
            'action' => 'colores/create',
            'colores_data' => $colores_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_colores/vw-colores',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Color',
            'button' => 'Crear',
            'action' => base_url().'/colores/create_action',
    	    'id_color' => '',
    	    'color_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_colores/fm-colores',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $id = $this->coloresModel->insert([
            'color_descripcion' => $this->request->getPost('color_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('colores'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();

        $color = $this->coloresModel->where('id_color',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Color',
            'button' => 'Actualizar',
            'action' => base_url().'/colores/edit_action',
    	    'id_color' => $color['id_color'],
    	    'color_descripcion' => $color['color_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_colores/fm-colores',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_color');
        $this->coloresModel->where('id_color', $id)->set([
            'color_descripcion' => $this->request->getPost('color_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('colores'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $color = $this->coloresModel->where('id_color',$id)->first();
        if ($color) {
            $this->coloresModel->where('id_color', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('colores'));
        }else{
            return redirect()->to(base_url('colores'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $color= $this->coloresModel->where('id_color',$id)->first();
        if ($color) {
            $this->coloresModel->where('id_color', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('colores'));
        }else{
            return redirect()->to(base_url('colores'));
        }
    }
}