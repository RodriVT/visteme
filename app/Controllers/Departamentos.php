<?php namespace App\Controllers;

class Departamentos extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();

        $departamentos = $this->departamentosModel->findAll();

        $departamentos_data = array();
        foreach($departamentos as $departamento){
            $acciones = array();
            switch ($departamento['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/departamentos/edit/'.$departamento['id_departamento'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/departamentos/baja/'.$departamento['id_departamento'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/departamentos/alta/'.$departamento['id_departamento'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $departamentos_data[] = array(
                'id_departamento'          => $departamento['id_departamento'],
                'departamento_descripcion'          => $departamento['departamento_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Departamentos',
            'action' => 'departamentos/create',
            'departamentos_data' => $departamentos_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_departamentos/vw-departamentos',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Departamento',
            'button' => 'Crear',
            'action' => base_url().'/departamentos/create_action',
    	    'id_departamento' => '',
    	    'departamento_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_departamentos/fm-departamentos',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $id = $this->departamentosModel->insert([
            'departamento_descripcion' => $this->request->getPost('departamento_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('departamentos'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();

        $departamento = $this->departamentosModel->where('id_departamento',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Departamento',
            'button' => 'Actualizar',
            'action' => base_url().'/departamentos/edit_action',
    	    'id_departamento' => $departamento['id_departamento'],
    	    'departamento_descripcion' => $departamento['departamento_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_departamentos/fm-departamentos',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_departamento');
        $this->departamentosModel->where('id_departamento', $id)->set([
            'departamento_descripcion' => $this->request->getPost('departamento_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('departamentos'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $departamento = $this->departamentosModel->where('id_departamento',$id)->first();
        if ($departamento) {
            $this->departamentosModel->where('id_departamento', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('departamentos'));
        }else{
            return redirect()->to(base_url('departamentos'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $departamento = $this->departamentosModel->where('id_departamento',$id)->first();
        if ($departamento) {
            $this->departamentosModel->where('id_departamento', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('departamentos'));
        }else{
            return redirect()->to(base_url('departamentos'));
        }
    }
}