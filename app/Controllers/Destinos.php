<?php namespace App\Controllers;

class Destinos extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $destinos = $this->destinosModel->findAll();

        $destinos_data = array();
        foreach($destinos as $destino){
            $acciones = array();
            switch ($destino['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/destinos/edit/'.$destino['id_destino'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/destinos/baja/'.$destino['id_destino'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/destinos/alta/'.$destino['id_destino'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $destinos_data[] = array(
                'id_destino'          => $destino['id_destino'],
                'destino_descripcion'          => $destino['destino_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Destinos',
            'action' => 'destinos/create',
            'destinos_data' => $destinos_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_destinos/vw-destinos',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Destino',
            'button' => 'Crear',
            'action' => base_url().'/destinos/create_action',
    	    'id_destino' => '',
    	    'destino_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_destinos/fm-destinos',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $id = $this->destinosModel->insert([
            'destino_descripcion' => $this->request->getPost('destino_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('destinos'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();
        $destino = $this->destinosModel->where('id_destino',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Destino',
            'button' => 'Actualizar',
            'action' => base_url().'/destinos/edit_action',
    	    'id_destino' => $destino['id_destino'],
    	    'destino_descripcion' => $destino['destino_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_destinos/fm-destinos',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_destino');
        $this->destinosModel->where('id_destino', $id)->set([
            'destino_descripcion' => $this->request->getPost('destino_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('destinos'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $destino = $this->destinosModel->where('id_destino',$id)->first();
        if ($destino) {
            $this->destinosModel->where('id_destino', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('destinos'));
        }else{
            return redirect()->to(base_url('destinos'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $destino = $this->destinosModel->where('id_destino',$id)->first();
        if ($destino) {
            $this->destinosModel->where('id_destino', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('destinos'));
        }else{
            return redirect()->to(base_url('destinos'));
        }
    }
}