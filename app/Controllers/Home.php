<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\UserModel;

class Home extends BaseController
{
	public function index()
	{
		$session = $this->validaSesion();

		$data = [
            'session' => $session
        ];

		echo view('vw-header',$data);
        echo view('vw-footer',$data);
	}
}
