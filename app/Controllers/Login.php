<?php namespace App\Controllers;

class Login extends BaseController
{
	public function index()
	{
        $data = [
            'action' => 'verifylogin/login',
            'action_movil' => 'verifylogin/login_movil',
        ];

        echo view('vw-login',$data);
    }
    public function logout()
	{
        $session = \Config\Services::session($config);
        $session->destroy();
        return '200';
	}
}
