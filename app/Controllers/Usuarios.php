<?php

namespace App\Controllers;

class Usuarios extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $usuarios = $this->usuariosModel->findAll();

        $usuarios_data = array();
        foreach($usuarios as $usuario){
            $acciones = array();
            switch ($usuario['id_estatus']) {
                case 1:
                    $acciones[] = array(
                        'href'  => base_url().'/usuarios/edit/'.$usuario['id_usuario'],
                        'class' => 'btn-primary mr-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro',
                        'data' => '',
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/usuarios/baja/'.$usuario['id_usuario'],
                        'class' => 'btn-danger ml-1 mr-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro',
                        'data' => '',
                    );
                    $acciones[] = array(
                        'href'  => '#',
                        'class' => 'btn-success ml-1 mr-1 restore-password',
                        'icon'  => 'fas fa-key',
                        'title' => 'Restablecer Contraseña',
                        'data' => $usuario['id_usuario'],
                    );
                    if($usuario['intentos'] == 3){
                        $acciones[] = array(
                            'href'  => base_url().'/usuarios/desbaneo/'.$usuario['id_usuario'],
                            'class' => 'btn-warning ml-1',
                            'icon'  => 'fas fa-lock-open',
                            'title' => 'Desbanear cuenta',
                            'data' => ''
                        );
                    }
                    break;
                default:
                    $acciones[] = array(
                        'href'  => base_url().'/usuarios/alta/'.$usuario['id_usuario'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro',
                        'data' => '',
                    );
                    break;
            }
            $perfil = $this->perfilesModel->where('id_perfil',$usuario['id_perfil'])->first();
            $usuarios_data[] = [
                'id_usuario'          => $usuario['id_usuario'],
                'perfil_descripcion'          => $perfil['perfil_descripcion'],
                'username'          => $usuario['username'],
                'nombre'          => $usuario['nombre'],
                'ape_paterno'          => $usuario['ape_paterno'],
                'ape_materno'          => $usuario['ape_materno'],
                'email'          => $usuario['email'],
                'acciones'          => $acciones
            ];
        }

        $data = [
            'base_url' => base_url(),
            'session' => $session,
            'title' => 'Lista de Usuarios',
            'action' => 'usuarios/create',
            'usuarios_data' => $usuarios_data,
        ];

        echo view('vw-header',$data);
        echo view('usuarios/vw-usuarios',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();
        $perfiles = $this->perfilesModel->findAll();
        $perfiles = $this->get_perfiles($perfiles,0);

        $data = [
            'session' => $session,
            'base_url' => base_url(),
            'title' => 'Creación Usuario',
            'button' => 'Crear',
            'action' => base_url().'/usuarios/create_action',
    	    'id_usuario' => '',
    	    'id_perfil_v' => '',
    	    'username' => '',
    	    'required_password' => 'required',
    	    'disabled_password' => '',
    	    'nombre' => '',
    	    'ape_paterno' => '',
    	    'ape_materno' => '',
    	    'email' => '',
            'foto_perfil' => '',
            'foto_perfil_old' => '',
            'perfiles' => $perfiles,
        ];

        echo view('vw-header',$data);
        echo view('usuarios/fm-usuarios',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $fileName = '';
        $path = 0;

        if($this->request->getPost('img_cam') == ''){
            if($this->request->getPost('foto_perfil_new') != ''){
                if($_FILES['file']['name'] != ''){
                    $fecha = date("YmdHis");
                    $fileName = $fecha.'_'.$_FILES['file']['name'];
                    $fileTmpLoc = $_FILES['file']['tmp_name'];
                    $path = 1;
                }
            }
        }else{
            $baseFromJavascript = $this->request->getPost('img_cam');
            $base_to_php = explode(',', $baseFromJavascript);
            $data = base64_decode($base_to_php[1]);

            $fecha = date("YmdHis");
            $fileName = $fecha.'_imageuser'.$session['id_user'].'.png';
            $path = 2;
        }

        $opciones = ['cost' => 10];
        $password = password_hash($this->request->getPost('password'), PASSWORD_BCRYPT, $opciones);

        $id_usuario = $this->usuariosModel->insert([
            'id_perfil' => $this->request->getPost('id_perfil'),
    	    'username' => $this->request->getPost('username'),
    	    'password' => $password,
    	    'nombre' => $this->request->getPost('nombre'),
    	    'ape_paterno' => $this->request->getPost('ape_paterno'),
    	    'ape_materno' => $this->request->getPost('ape_materno'),
    	    'foto_perfil' => $fileName,
    	    'email' => $this->request->getPost('email'),
    	    'telefono' => $this->request->getPost('telefono'),
    	    'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
            'intentos' => 0
        ]);

        if($path != 0){
            $dir = 'assets/img/img_users/'.$id_usuario;
            if($path == 1){
                $destino = 'assets/img/img_users/'.$id_usuario.'/'.$fileName;
                if(!file_exists($dir)){
                    mkdir($dir, 0777, true);
                };
                if(move_uploaded_file($fileTmpLoc, $destino)){
                    echo $fileName." movido correctamente";
                }else{
                    echo "No se ha podido mover el archivo: ".$fileName;
                }
            }else if($path == 2){
                $filepath = "../assets/img/img_users/".$id_usuario."/".$fileName;
                if(!file_exists($dir)){
                    mkdir($dir, 0777, true);
                };
                file_put_contents($filepath, $data);
            }
        }

        return redirect()->to(base_url('usuarios'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();
        $usuario = $this->usuariosModel->where('id_usuario',$id)->first();
        $perfiles = $this->perfilesModel->findAll();
        $perfiles = $this->get_perfiles($perfiles,$usuario['id_perfil']);

        $data = [
            'session' => $session,
            'base_url' => base_url(),
            'title' => 'Actualizar Usuario',
            'button' => 'Actualizar',
            'action' => base_url().'/usuarios/edit_action',
    	    'id_usuario' => $usuario['id_usuario'],
    	    'id_perfil_v' => $usuario['id_perfil'],
            'username' => $usuario['username'],
            'required_password' => '',
    	    'disabled_password' => 'd-none',
    	    'nombre' => $usuario['nombre'],
    	    'ape_paterno' => $usuario['ape_paterno'],
    	    'ape_materno' => $usuario['ape_materno'],
    	    'email' => $usuario['email'],
            'telefono' => $usuario['telefono'],
            'foto_perfil' => $usuario['foto_perfil'],
            'foto_perfil_old' => $usuario['foto_perfil'],
            'perfiles' => $perfiles,
        ];

        echo view('vw-header',$data);
        echo view('usuarios/fm-usuarios',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_usuario');

        $fileName = $this->request->getPost('foto_perfil');

        if($this->request->getPost('img_cam') == ''){
            if($this->request->getPost('foto_perfil') == ''){
                if($_FILES['file']['name'] != ''){
                    $fecha = date("YmdHis");
                    $fileName = $fecha.'_'.$_FILES['file']['name'];
                    $fileTmpLoc = $_FILES['file']['tmp_name'];

                    $destino = 'assets/img/img_users/'.$id.'/'.$fileName;
                    if(move_uploaded_file($fileTmpLoc, $destino)){
                        echo $fileName." movido correctamente";
                    }else{
                        echo "No se ha podido mover el archivo: ".$fileName;
                    }
                }
                $url_promotional_old = $this->request->getPost('foto_perfil_old');
                if($url_promotional_old != ''){
                    $delete_local = 'assets/img/img_users/'.$id.'/'.$url_promotional_old;
                    unlink($delete_local);
                }
            }
        }else{
            $baseFromJavascript = $this->request->getPost('img_cam');
            $base_to_php = explode(',', $baseFromJavascript);
            $data = base64_decode($base_to_php[1]);

            $fecha = date("YmdHis");
            $fileName = $fecha.'_imageuser'.$id.'.png';

            $filepath = 'assets/img/img_users/'.$id.'/'.$fileName;

            file_put_contents($filepath, $data);
            $url_promotional_old = $this->request->getPost('foto_perfil_old');
            if($url_promotional_old != ''){
                $delete_local = 'assets/img/img_users/'.$id.'/'.$url_promotional_old;
                unlink($delete_local);
            }
        }

        $this->usuariosModel->where('id_usuario', $id)->set([
            'id_perfil' => $this->request->getPost('id_perfil'),
    	    'username' => $this->request->getPost('username'),
    	    'nombre' => $this->request->getPost('nombre'),
    	    'ape_paterno' => $this->request->getPost('ape_paterno'),
    	    'ape_materno' => $this->request->getPost('ape_materno'),
    	    'foto_perfil' => $fileName,
    	    'email' => $this->request->getPost('email'),
    	    'telefono' => $this->request->getPost('telefono'),
    	    'id_estatus' => 1,
            'updater_user_id' => $session['id_usuario_session'],
            'intentos' => 0
        ])->update();

        return redirect()->to(base_url('usuarios'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $usuario = $this->usuariosModel->where('id_usuario',$id)->first();
        if ($usuario) {
            $this->usuariosModel->where('id_usuario', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('usuarios'));
        }else{
            return redirect()->to(base_url('usuarios'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $usuario = $this->usuariosModel->where('id_usuario',$id)->first();
        if ($usuario) {
            $this->usuariosModel->where('id_usuario', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('usuarios'));
        }else{
            return redirect()->to(base_url('usuarios'));
        }
    }

    public function desbaneo($id)
    {
        $session = $this->validaSesion();

        $usuario = $this->usuariosModel->where('id_usuario',$id)->first();
        if ($usuario) {
            $this->usuariosModel->where('id_usuario', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'intentos' => 0])->update();
            return redirect()->to(base_url('usuarios'));
        }else{
            return redirect()->to(base_url('usuarios'));
        }
    }

    public function validate_email()
    {
        $session = $this->validaSesion();
        $email = $this->request->getPost('email');
        $user = $this->usuariosModel->where('email',$email)->first();
        echo json_encode($user);
    }
}
