<?php
namespace App\Controllers;


class Categorias extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();

        $categorias = $this->categoriasModel->findAll();

        $categorias_data = array();
        foreach($categorias as $categoria){
            $acciones = array();
            switch ($categoria['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/categorias/edit/'.$categoria['id_categoria'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/categorias/baja/'.$categoria['id_categoria'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/categorias/alta/'.$categoria['id_categoria'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $categorias_data[] = array(
                'id_categoria'          => $categoria['id_categoria'],
                'categoria_descripcion'          => $categoria['categoria_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Categorías',
            'action' => 'categorias/create',
            'categorias_data' => $categorias_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_categorias/vw-categorias',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Categoría',
            'button' => 'Crear',
            'action' => base_url().'/categorias/create_action',
    	    'id_categoria' => '',
    	    'categoria_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_categorias/fm-categorias',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $id = $this->categoriasModel->insert([
            'categoria_descripcion' => $this->request->getPost('categoria_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('categorias'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();

        $categoria = $this->categoriasModel->where('id_categoria',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Marca',
            'button' => 'Actualizar',
            'action' => base_url().'/categorias/edit_action',
    	    'id_categoria' => $categoria['id_categoria'],
    	    'categoria_descripcion' => $categoria['categoria_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_categorias/fm-categorias',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_categoria');
        $this->categoriasModel->where('id_categoria', $id)->set([
            'categoria_descripcion' => $this->request->getPost('categoria_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('categorias'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $categoria = $this->categoriasModel->where('id_categoria',$id)->first();
        if ($categoria) {
            $this->categoriasModel->where('id_categoria', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('categorias'));
        }else{
            return redirect()->to(base_url('categorias'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $categoria = $this->categoriasModel->where('id_categoria',$id)->first();
        if ($categoria) {
            $this->categoriasModel->where('id_categoria', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('categorias'));
        }else{
            return redirect()->to(base_url('categorias'));
        }
    }
}