<?php

namespace App\Controllers;

class Opciones_menu extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();

        $opciones_menu = $this->opcionesMenuModel->findAll();

        $opciones_menu_data = array();
        foreach($opciones_menu as $opcion_menu){
            $acciones = array();
            switch ($opcion_menu['id_estatus']) {
                case 1:
                    $acciones[] = array(
                        'href'  => base_url().'/opciones_menu/edit/'.$opcion_menu['id_menu'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/opciones_menu/baja/'.$opcion_menu['id_menu'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $acciones[] = array(
                        'href'  => base_url().'/opciones_menu/alta/'.$opcion_menu['id_menu'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $opciones_menu_data[] = array(
                'id_menu'          => $opcion_menu['id_menu'],
                'etiqueta'          => $opcion_menu['etiqueta'],
                'url'          => ($opcion_menu['url'] != null) ? $opcion_menu['url'] : '',
                'parent_menu_id'          => $opcion_menu['parent_menu_id'],
                'fa_icon'          => ($opcion_menu['fa_icon'] != null) ? $opcion_menu['fa_icon'] : '',
                'orden'          => $opcion_menu['orden'],
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista Opciones de Menú',
            'action' => 'opciones_menu/create',
            'opciones_menu_data' => $opciones_menu_data,
        ];

        echo view('vw-header',$data);
        echo view('opciones_menu/vw-opciones-menu',$data);
        echo view('vw-footer',$data);
    }

    public function create(){
        $session = $this->validaSesion();

        $opciones_menu = $this->opcionesMenuModel->where('parent_menu_id',0)->where('id_estatus',1)->findAll();
        $parentsMenu = $this->get_parents_menu($opciones_menu,0);

        $data = [
            'session' => $session,
            'title' => 'Creación Opción de Menú',
            'button' => 'Crear',
            'action' => base_url().'/opciones_menu/create_action',
    	    'id_menu' => '',
    	    'etiqueta' => '',
            'parent_menu_id' => '',
            'fa_icon' => '',
            'disabled_fa_icon' => '',
            'url' => '',
    	    'disabled_url' => 'disabled',
            'orden' => '',
            'list_parents_menu' => $parentsMenu,
        ];

        echo view('vw-header',$data);
        echo view('opciones_menu/fm-opciones-menu',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $this->opcionesMenuModel->insert([
            'etiqueta' => $this->request->getPost('etiqueta'),
    	    'parent_menu_id' => $this->request->getPost('parent_menu_id'),
    	    'fa_icon' => $this->request->getPost('fa_icon'),
    	    'url' => $this->request->getPost('url'),
    	    'orden' => $this->request->getPost('orden'),
    	    'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('opciones_menu'));
    }

    public function edit($id){
        $session = $this->validaSesion();
        $opcionMenu = $this->opcionesMenuModel->where('id_menu',$id)->first();
        $parentsMenu = $this->opcionesMenuModel->where('parent_menu_id',0)->where('id_estatus',1)->findAll();
        $parentsMenu = $this->get_parents_menu($parentsMenu,$opcionMenu['parent_menu_id']);

        $data = [
            'session' => $session,
            'title' => 'Actualizar Opción de Menú',
            'button' => 'Actualizar',
            'action' => base_url().'/opciones_menu/edit_action',
    	    'id_menu' => $opcionMenu['id_menu'],
    	    'etiqueta' => $opcionMenu['etiqueta'],
    	    'parent_menu_id' => $opcionMenu['parent_menu_id'],
            'fa_icon' => ($opcionMenu['fa_icon'] != null) ? $opcionMenu['fa_icon'] : '',
            'disabled_fa_icon' => ($opcionMenu['fa_icon'] != null) ? '' : 'disabled',
            'url' => ($opcionMenu['url'] != null) ? $opcionMenu['url'] : '',
    	    'disabled_url' => ($opcionMenu['url'] != null) ? '' : 'disabled',
    	    'orden' => $opcionMenu['orden'],
            'list_parents_menu' => $parentsMenu,
        ];

        echo view('vw-header',$data);
        echo view('opciones_menu/fm-opciones-menu',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_menu');
        $this->opcionesMenuModel->where('id_menu', $id)->set([
            'etiqueta' => $this->request->getPost('etiqueta'),
    	    'parent_menu_id' => $this->request->getPost('parent_menu_id'),
    	    'fa_icon' => $this->request->getPost('fa_icon'),
    	    'url' => $this->request->getPost('url'),
    	    'orden' => $this->request->getPost('orden'),
    	    'id_status' => 1,
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('opciones_menu'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $opcionMenu = $this->opcionesMenuModel->where('id_menu',$id)->first();
        if ($opcionMenu) {
            $this->opcionesMenuModel->where('id_menu', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('opciones_menu'));
        }else{
            return redirect()->to(base_url('opciones_menu'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $opcionMenu = $this->opcionesMenuModel->where('id_menu',$id)->first();
        if ($opcionMenu) {
            $this->opcionesMenuModel->where('id_menu', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('opciones_menu'));
        }else{
            return redirect()->to(base_url('opciones_menu'));
        }
    }

    public function get_parents_menu($array, $id){
        $data = array();
        foreach($array as $object){
            $data[] = array(
                'id_menu' => $object['id_menu'],
                'etiqueta' => $object['etiqueta'],
                'selected_parent' => ($id == $object['id_menu']) ? 'selected' : '',
            );
        }
        return $data;
    }
}
