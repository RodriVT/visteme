<?php namespace App\Controllers;

class Ocasiones extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $ocasiones = $this->ocasionesModel->findAll();

        $ocasiones_data = array();
        foreach($ocasiones as $ocasion){
            $acciones = array();
            switch ($ocasion['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/ocasiones/edit/'.$ocasion['id_ocasion'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/ocasiones/baja/'.$ocasion['id_ocasion'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/ocasiones/alta/'.$ocasion['id_ocasion'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $ocasiones_data[] = array(
                'id_ocasion'          => $ocasion['id_ocasion'],
                'ocasion_descripcion'          => $ocasion['ocasion_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Ocasiones',
            'action' => 'ocasiones/create',
            'ocasiones_data' => $ocasiones_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_ocasiones/vw-ocasiones',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Ocasión',
            'button' => 'Crear',
            'action' => base_url().'/ocasiones/create_action',
    	    'id_ocasion' => '',
    	    'ocasion_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_ocasiones/fm-ocasiones',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $id = $this->ocasionesModel->insert([
            'ocasion_descripcion' => $this->request->getPost('ocasion_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('ocasiones'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();
        $ocasion = $this->ocasionesModel->where('id_ocasion',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Ocasión',
            'button' => 'Actualizar',
            'action' => base_url().'/ocasiones/edit_action',
    	    'id_ocasion' => $ocasion['id_ocasion'],
    	    'ocasion_descripcion' => $ocasion['ocasion_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_ocasiones/fm-ocasiones',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->  validaSesion();

        $id = $this->request->getPost('id_ocasion');
        $this->ocasionesModel->where('id_ocasion', $id)->set([
            'ocasion_descripcion' => $this->request->getPost('ocasion_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('ocasiones'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $ocasion = $this->ocasionesModel->where('id_ocasion',$id)->first();
        if ($ocasion) {
            $this->ocasionesModel->where('id_ocasion', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('ocasiones'));
        }else{
            return redirect()->to(base_url('ocasiones'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $ocasion = $this->ocasionesModel->where('id_ocasion',$id)->first();
        if ($ocasion) {
            $this->ocasionesModel->where('id_ocasion', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('ocasiones'));
        }else{
            return redirect()->to(base_url('ocasiones'));
        }
    }
}