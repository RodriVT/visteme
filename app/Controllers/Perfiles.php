<?php namespace App\Controllers;

class Perfiles extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $perfiles = $this->perfilesModel->findAll();

        $perfiles_data = array();
        foreach($perfiles as $perfil){
            $acciones = array();
            switch ($perfil['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/perfiles/edit/'.$perfil['id_perfil'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/perfiles/baja/'.$perfil['id_perfil'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/perfiles/alta/'.$perfil['id_perfil'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $perfiles_data[] = array(
                'id_perfil'          => $perfil['id_perfil'],
                'perfil_descripcion'          => $perfil['perfil_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Perfiles',
            'action' => 'perfiles/create',
            'perfiles_data' => $perfiles_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_perfiles/vw-perfiles',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Perfil',
            'button' => 'Crear',
            'action' => base_url().'/perfiles/create_action',
    	    'id_perfil' => '',
    	    'perfil_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_perfiles/fm-perfiles',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $this->perfilesModel->insert([
            'perfil_descripcion' => $this->request->getPost('perfil_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('perfiles'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();
        $perfil = $this->perfilesModel->where('id_perfil',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Perfil',
            'button' => 'Actualizar',
            'action' => base_url().'/perfiles/edit_action',
    	    'id_perfil' => $perfil['id_perfil'],
    	    'perfil_descripcion' => $perfil['perfil_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_perfiles/fm-perfiles',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_perfil');
        $this->perfilesModel->where('id_perfil', $id)->set([
            'perfil_descripcion' => $this->request->getPost('perfil_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('perfiles'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $perfil = $this->perfilesModel->where('id_perfil',$id)->first();
        if ($perfil) {
            $this->perfilesModel->where('id_perfil', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('perfiles'));
        }else{
            return redirect()->to(base_url('perfiles'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $perfil = $this->perfilesModel->where('id_perfil',$id)->first();
        if ($perfil) {
            $this->perfilesModel->where('id_perfil', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('perfiles'));
        }else{
            return redirect()->to(base_url('perfiles'));
        }
    }
}