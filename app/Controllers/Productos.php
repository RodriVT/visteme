<?php namespace App\Controllers;

class Productos extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();

        $productos = $this->productosModel->findAll();
        $productos_data = array();
        foreach($productos as $producto){
            $acciones = array();
            switch ($producto['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/productos/edit/'.$producto['id_producto'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/productos/baja/'.$producto['id_producto'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/productos/alta/'.$producto['id_producto'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $productos_data[] = array(
                'id_producto'          => $producto['id_producto'],
                'nombre_producto'          => $producto['nombre_producto'],
                'sku'          => $producto['sku'],
                'code'          => $producto['code'],
                'precio'          => $producto['precio'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Productos',
            'action' => 'productos/create',
            'productos_data' => $productos_data,
        ];

        echo view('vw-header',$data);
        echo view('productos/vw-productos',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $marcas = $this->marcasModel->where('id_estatus',1)->find();
        $departamentos = $this->departamentosModel->where('id_estatus',1)->find();
        $subcategorias = $this->subcategoriasModel->where('id_estatus',1)->find();
        $materiales = $this->materialesModel->where('id_estatus',1)->find();
        $ocasiones = $this->ocasionesModel->where('id_estatus',1)->find();
        $colores = $this->coloresModel->where('id_estatus',1)->find();
        $tamanios = $this->tamaniosModel->where('id_estatus',1)->find();
        $tipos = $this->tiposModel->where('id_estatus',1)->find();

        $data = [
            'session' => $session,
            'title' => 'Crear Producto',
            'button' => 'Crear',
    	    'id_producto' => '',
            'keyprod' => '',
            'nombre_producto' => '',
    	    'sku' => '',
            'code' => '',
            'precio' => '',
            'id_marca' => '',
            'id_departamento' => '',
            'id_subcategoria' => '',
            'id_material' => '',
            'id_ocasion' => '',
            'id_tipo' => '',
            'id_color' => '',
            'id_tamanio' => '',
            'cantidad' => '',
            'marcas' => $marcas,
            'departamentos' => $departamentos,
            'subcategorias' => $subcategorias,
            'materiales' => $materiales,
            'ocasiones' => $ocasiones,
            'colores' => $colores,
            'tamanios' => $tamanios,
            'tipos' => $tipos
        ];

        echo view('vw-header',$data);
        echo view('productos/fm-productos',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $key_validate = false;
        $keyprod = 'NO VALIDA';
        while($key_validate == false){
            $keyprod = $this->generateRandomString(32);
            $restaurant = $this->productosModel->where('keyprod',$keyprod)->first();
            if($restaurant == null){
                $key_validate = true;
            }
        }

        if($_FILES['imagenes']){
            for($i=0; $i < count($_FILES['imagenes']['name']); $i++){
                $fileTmpLoc = $_FILES['imagenes']['tmp_name'][$i];

                $key_validate_img = false;
                while($key_validate_img == false){
                    $keyimg = $this->generateRandomString(32);
                    $imagen = $this->productoImagenesModel->where('keyimg',$keyimg)->first();
                    if($imagen == null){
                        $key_validate_img = true;
                    }
                }
                $name = $_FILES['imagenes']['name'][$i];
                $extension = strtolower(substr(strrchr($name, "."), 1));

                $url_image = $keyimg.'.'.$extension;

                $this->productoImagenesModel->insert([
                    'keyprod' => $keyprod,
                    'keyimg' => $url_image,
                    'id_estatus' => 1,
                    'creator_user_id' => $session['id_usuario_session'],
                ]);

                $destino = 'assets/img/products/'.$url_image;
                if(move_uploaded_file($fileTmpLoc, $destino)){
                    echo $fileName." movido correctamente";
                }else{
                    echo "No se ha podido mover el archivo: ".$fileName;
                }
            }
        }

        $this->productosModel->insert([
            'nombre_producto' => $this->request->getPost('nombre_producto'),
    	    'sku' => $this->request->getPost('sku'),
            'code' => $this->request->getPost('code'),
    	    'precio' => $this->request->getPost('precio'),
    	    'id_marca' => $this->request->getPost('id_marca'),
    	    'id_departamento' => $this->request->getPost('id_departamento'),
    	    'id_subcategoria' => $this->request->getPost('id_subcategoria'),
    	    'id_material' => $this->request->getPost('id_material'),
    	    'id_ocasion' => $this->request->getPost('id_ocasion'),
            'id_tipo' => $this->request->getPost('id_tipo'),
            'id_color' => $this->request->getPost('id_color'),
            'id_tamanio' => $this->request->getPost('id_tamanio'),
            'cantidad' => $this->request->getPost('cantidad'),
    	    'keyprod' => $keyprod,
    	    'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);
    }

    public function edit($id)
    {
        $session = $this->validaSesion();

        $marcas = $this->marcasModel->where('id_estatus',1)->find();
        $departamentos = $this->departamentosModel->where('id_estatus',1)->find();
        $subcategorias = $this->subcategoriasModel->where('id_estatus',1)->find();
        $materiales = $this->materialesModel->where('id_estatus',1)->find();
        $ocasiones = $this->ocasionesModel->where('id_estatus',1)->find();
        $colores = $this->coloresModel->where('id_estatus',1)->find();
        $tamanios = $this->tamaniosModel->where('id_estatus',1)->find();
        $tipos = $this->tiposModel->where('id_estatus',1)->find();
        $producto = $this->productosModel->where('id_producto',$id)->first();
        $images_data = $this->productoImagenesModel->where('keyprod',$producto['keyprod'])->findAll();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Producto',
            'button' => 'Actualizar',
            'url_image' => '//visteme.local',
    	    'id_producto' => $producto['id_producto'],
    	    'nombre_producto' => $producto['nombre_producto'],
            'sku' => $producto['sku'],
            'code' => $producto['code'],
            'precio' => $producto['precio'],
            'cantidad' => $producto['cantidad'],
            'id_marca' => $producto['id_marca'],
            'id_departamento' => $producto['id_departamento'],
            'id_subcategoria' => $producto['id_subcategoria'],
            'id_material' => $producto['id_material'],
            'id_ocasion' => $producto['id_ocasion'],
            'id_tipo' => $producto['id_tipo'],
            'id_color' => $producto['id_color'],
            'id_tamanio' => $producto['id_tamanio'],
            'keyprod' => $producto['keyprod'],
    	    'images_data' => $images_data,
            'marcas' => $marcas,
            'departamentos' => $departamentos,
            'subcategorias' => $subcategorias,
            'materiales' => $materiales,
            'ocasiones' => $ocasiones,
            'colores' => $colores,
            'tamanios' => $tamanios,
            'tipos' => $tipos
        ];

        echo view('vw-header',$data);
        echo view('productos/fm-edit-productos',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_producto');
        $first_img = true;

        if($_FILES['imagenes_news']){
            for($i=0; $i < count($_FILES['imagenes_news']['name']); $i++){
                $fileTmpLoc = $_FILES['imagenes_news']['tmp_name'][$i];

                $key_validate_img = false;
                while($key_validate_img == false){
                    $keyimg = $this->generateRandomString(32);
                    $imagen = $this->productoImagenesModel->where('keyimg',$keyimg)->first();
                    if($imagen == null){
                        $key_validate_img = true;
                    }
                }
                $name = $_FILES['imagenes_news']['name'][$i];
                $extension = strtolower(substr(strrchr($name, "."), 1));

                $url_image = $keyimg.'.'.$extension;

                if($first_img == true){
                    $first_img = false;
                    $imagen1 = $url_image;
                }
                $this->productoImagenesModel->insert([
                    'keyprod' => $this->request->getPost('keyprod'),
                    'keyimg' => $url_image,
                    'id_estatus' => 1,
                    'creator_user_id' => $session['id_usuario_session'],
                ]);

                $destino = 'assets/img/products/'.$url_image;
                if(move_uploaded_file($fileTmpLoc, $destino)){
                    echo $fileName." movido correctamente";
                }else{
                    echo "No se ha podido mover el archivo: ".$fileName;
                }
            }
        }

        $this->productosModel->where('id_producto', $id)->set([
            'nombre_producto' => $this->request->getPost('nombre_producto'),
    	    'sku' => $this->request->getPost('sku'),
            'code' => $this->request->getPost('code'),
    	    'precio' => $this->request->getPost('precio'),
    	    'id_marca' => $this->request->getPost('id_marca'),
    	    'id_departamento' => $this->request->getPost('id_departamento'),
    	    'id_subcategoria' => $this->request->getPost('id_subcategoria'),
    	    'id_material' => $this->request->getPost('id_material'),
    	    'id_ocasion' => $this->request->getPost('id_ocasion'),
            'id_tipo' => $this->request->getPost('id_tipo'),
            'id_color' => $this->request->getPost('id_color'),
            'id_tamanio' => $this->request->getPost('id_tamanio'),
            'cantidad' => $this->request->getPost('cantidad'),
    	    'keyprod' => $this->request->getPost('keyprod'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        if($_POST['imagenes_down']){
            foreach($_POST['imagenes_down'] as $image){
                $galeria = $this->productoImagenesModel->where('id_producto_imagen',$image)->first();
                $url_delete = 'assets/img/products/'.$galeria['keyimg'];
                unlink($url_delete);
                $this->productoImagenesModel->where('id_producto_imagen', $image)->delete();
            }
        }
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $producto = $this->productosModel->where('id_producto',$id)->first();
        if ($producto) {
            $this->productosModel->where('id_producto', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('productos'));
        }else{
            return redirect()->to(base_url('productos'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $producto = $this->productosModel->where('id_producto',$id)->first();
        if ($producto) {
            $this->productosModel->where('id_producto', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('productos'));
        }else{
            return redirect()->to(base_url('productos'));
        }
    }
}