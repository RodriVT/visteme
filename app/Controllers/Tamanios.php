<?php namespace App\Controllers;

class Tamanios extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $tamanios = $this->tamaniosModel->findAll();

        $tamanios_data = array();
        foreach($tamanios as $tamanio){
            $acciones = array();
            switch ($tamanio['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/tamanios/edit/'.$tamanio['id_tamanio'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/tamanios/baja/'.$tamanio['id_tamanio'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/tamanios/alta/'.$tamanio['id_tamanio'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $tamanios_data[] = array(
                'id_tamanio'          => $tamanio['id_tamanio'],
                'tamanio_descripcion'          => $tamanio['tamanio_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Tamaños',
            'action' => 'tamanios/create',
            'tamanios_data' => $tamanios_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_tamanios/vw-tamanios',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Tamaño',
            'button' => 'Crear',
            'action' => base_url().'/tamanios/create_action',
    	    'id_tamanio' => '',
    	    'tamanio_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_tamanios/fm-tamanios',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $this->tamaniosModel->insert([
            'tamanio_descripcion' => $this->request->getPost('tamanio_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('tamanios'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();
        $tamanio = $this->tamaniosModel->where('id_tamanio',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Tamaño',
            'button' => 'Actualizar',
            'action' => base_url().'/tamanios/edit_action',
    	    'id_tamanio' => $tamanio['id_tamanio'],
    	    'tamanio_descripcion' => $tamanio['tamanio_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_tamanios/fm-tamanios',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_tamanio');
        $this->tamaniosModel->where('id_tamanio', $id)->set([
            'tamanio_descripcion' => $this->request->getPost('tamanio_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('tamanios'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $tamanio = $this->tamaniosModel->where('id_tamanio',$id)->first();
        if ($tamanio) {
            $this->tamaniosModel->where('id_tamanio', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('tamanios'));
        }else{
            return redirect()->to(base_url('tamanios'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $tamanio = $this->tamaniosModel->where('id_tamanio',$id)->first();
        if ($tamanio) {
            $this->tamaniosModel->where('id_tamanio', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('tamanios'));
        }else{
            return redirect()->to(base_url('tamanios'));
        }
    }
}