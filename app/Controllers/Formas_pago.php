<?php namespace App\Controllers;

class Formas_pago extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $formas_pago = $this->formasPagoModel->findAll();

        $formas_pago_data = array();
        foreach($formas_pago as $forma_pago){
            $acciones = array();
            switch ($forma_pago['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/formas_pago/edit/'.$forma_pago['id_forma_pago'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/formas_pago/baja/'.$forma_pago['id_forma_pago'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/formas_pago/alta/'.$forma_pago['id_forma_pago'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $formas_pago_data[] = array(
                'id_forma_pago'          => $forma_pago['id_forma_pago'],
                'forma_pago_descripcion'          => $forma_pago['forma_pago_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Formas Pago',
            'action' => 'formas_pago/create',
            'formas_pago_data' => $formas_pago_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_formas_pago/vw-formas-pago',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Forma Pago',
            'button' => 'Crear',
            'action' => base_url().'/formas_pago/create_action',
    	    'id_forma_pago' => '',
    	    'forma_pago_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_formas_pago/fm-formas-pago',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $id = $this->formasPagoModel->insert([
            'forma_pago_descripcion' => $this->request->getPost('forma_pago_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('formas_pago'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();
        $forma_pago = $this->formasPagoModel->where('id_forma_pago',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Forma Pago',
            'button' => 'Actualizar',
            'action' => base_url().'/formas_pago/edit_action',
    	    'id_forma_pago' => $forma_pago['id_forma_pago'],
    	    'forma_pago_descripcion' => $forma_pago['forma_pago_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_formas_pago/fm-formas-pago',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_forma_pago');
        $this->formasPagoModel->where('id_forma_pago', $id)->set([
            'forma_pago_descripcion' => $this->request->getPost('forma_pago_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('formas_pago'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $forma_pago = $this->formasPagoModel->where('id_forma_pago',$id)->first();
        if ($forma_pago) {
            $this->formasPagoModel->where('id_forma_pago', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('formas_pago'));
        }else{
            return redirect()->to(base_url('formas_pago'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $forma_pago = $this->formasPagoModel->where('id_forma_pago',$id)->first();
        if ($forma_pago) {
            $this->formasPagoModel->where('id_forma_pago', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('formas_pago'));
        }else{
            return redirect()->to(base_url('formas_pago'));
        }
    }
}