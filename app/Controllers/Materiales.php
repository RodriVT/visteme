<?php namespace App\Controllers;

class Materiales extends BaseController
{
	public function index()
	{
        $session = $this->validaSesion();
        $materiales = $this->materialesModel->findAll();

        $materiales_data = array();
        foreach($materiales as $material){
            $acciones = array();
            switch ($material['id_estatus']) {
                case 1:
                    $estatus= 'Activo';
                    $acciones[] = array(
                        'href'  => base_url().'/materiales/edit/'.$material['id_material'],
                        'class' => 'btn-primary ml-1',
                        'icon'  => 'fas fa-pencil-alt',
                        'title' => 'Editar Registro'
                    );
                    $acciones[] = array(
                        'href'  => base_url().'/materiales/baja/'.$material['id_material'],
                        'class' => 'btn-danger ml-1',
                        'icon'  => 'fas fa-trash',
                        'title' => 'Desactivar Registro'
                    );
                    break;
                default:
                    $estatus= 'Baja';
                    $acciones[] = array(
                        'href'  => base_url().'/materiales/alta/'.$material['id_material'],
                        'class' => 'btn-warning',
                        'icon'  => 'fas fa-check',
                        'title' => 'Activar Registro'
                    );
                    break;
            }
            $materiales_data[] = array(
                'id_material'          => $material['id_material'],
                'material_descripcion'          => $material['material_descripcion'],
                'estatus'           => $estatus,
                'acciones'          => $acciones
            );
        }

        $data = [
            'session' => $session,
            'title' => 'Lista de Materiales',
            'action' => 'materiales/create',
            'materiales_data' => $materiales_data,
        ];

        echo view('vw-header',$data);
        echo view('cat_materiales/vw-materiales',$data);
        echo view('vw-footer',$data);
    }

    public function create()
    {
        $session = $this->validaSesion();

        $data = [
            'session' => $session,
            'title' => 'Crear Material',
            'button' => 'Crear',
            'action' => base_url().'/materiales/create_action',
    	    'id_material' => '',
    	    'material_descripcion' => '',
        ];

        echo view('vw-header',$data);
        echo view('cat_materiales/fm-materiales',$data);
        echo view('vw-footer',$data);
    }

    public function create_action()
    {
        $session = $this->validaSesion();

        $id = $this->materialesModel->insert([
            'material_descripcion' => $this->request->getPost('material_descripcion'),
            'id_estatus' => 1,
            'creator_user_id' => $session['id_usuario_session'],
        ]);

        return redirect()->to(base_url('materiales'));
    }

    public function edit($id)
    {
        $session = $this->validaSesion();
        $material = $this->materialesModel->where('id_material',$id)->first();

        $data = [
            'session' => $session,
            'title' => 'Actualizar Material',
            'button' => 'Actualizar',
            'action' => base_url().'/materiales/edit_action',
    	    'id_material' => $material['id_material'],
    	    'material_descripcion' => $material['material_descripcion']
        ];

        echo view('vw-header',$data);
        echo view('cat_materiales/fm-materiales',$data);
        echo view('vw-footer',$data);
    }

    public function edit_action()
    {
        $session = $this->validaSesion();

        $id = $this->request->getPost('id_material');
        $this->materialesModel->where('id_material', $id)->set([
            'material_descripcion' => $this->request->getPost('material_descripcion'),
            'updater_user_id' => $session['id_usuario_session'],
        ])->update();

        return redirect()->to(base_url('materiales'));
    }

    public function alta($id)
    {
        $session = $this->validaSesion();

        $material = $this->materialesModel->where('id_material',$id)->first();
        if ($material) {
            $this->materialesModel->where('id_material', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 1])->update();
            return redirect()->to(base_url('materiales'));
        }else{
            return redirect()->to(base_url('materiales'));
        }
    }

    public function baja($id)
    {
        $session = $this->validaSesion();

        $material = $this->materialesModel->where('id_material',$id)->first();
        if ($material) {
            $this->materialesModel->where('id_material', $id)->set(['updater_user_id' => $session['id_usuario_session'], 'id_estatus' => 2])->update();
            return redirect()->to(base_url('materiales'));
        }else{
            return redirect()->to(base_url('materiales'));
        }
    }
}