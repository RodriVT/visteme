<?php namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;
use App\Models\Opciones_menu_model;
use App\Models\Permisos_model;
use App\Models\Categorias_model;
use App\Models\Productos_model;
use App\Models\Colores_model;
use App\Models\Departamentos_model;
use App\Models\Destinos_model;
use App\Models\Formas_envio_model;
use App\Models\Formas_pago_model;
use App\Models\Marcas_model;
use App\Models\Materiales_model;
use App\Models\Ocasiones_model;
use App\Models\Perfiles_model;
use App\Models\Subcategorias_model;
use App\Models\Tamanios_model;
use App\Models\Tipos_model;
use App\Models\Usuarios_model;
use App\Models\Producto_imagenes_model;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [ ];

	/**
	 * Constructor.
	 *
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();

        $this->coloresModel = new Colores_model($db);
        $this->categoriasModel = new Categorias_model($db);
        $this->departamentosModel = new Departamentos_model($db);
        $this->destinosModel = new Destinos_model($db);
        $this->formasEnvioModel = new Formas_envio_model($db);
        $this->formasPagoModel = new Formas_pago_model($db);
        $this->marcasModel = new Marcas_model($db);
        $this->materialesModel = new Materiales_model($db);
        $this->ocasionesModel = new Ocasiones_model($db);
        $this->opcionesMenuModel = new Opciones_menu_model($db);
        $this->perfilesModel = new Perfiles_model($db);
        $this->permisosModel = new Permisos_model($db);
        $this->productoImagenesModel = new Producto_imagenes_model($db);
        $this->productosModel = new Productos_model($db);
        $this->subcategoriasModel = new Subcategorias_model($db);
        $this->tamaniosModel = new Tamanios_model($db);
        $this->tiposModel = new Tipos_model($db);
        $this->usuariosModel = new Usuarios_model($db);
	}

	public function validaSesion(){
        $session = \Config\Services::session($config);
        if (isset($_SESSION['logged_in']))
        {
            $permisosModel = new Permisos_model($db);
            $opcionesMenuModel = new Opciones_menu_model($db);
            $permisos = $permisosModel->where('id_perfil',$_SESSION['logged_in']['id_perfil'])->where('id_estatus',1)->findAll();
            $menu = null;
            foreach($permisos as $permiso){
                $parent = $opcionesMenuModel->where('id_menu',$permiso['id_menu'])->first();
                if($parent['parent_menu_id'] == 0){
                    $children_menu = null;
                    $children_parent = $opcionesMenuModel->where('parent_menu_id',$parent['id_menu'])->findAll();
                    foreach($children_parent as $son_parent){
                        $validate_son = false;
                        foreach($permisos as $permiso){
                            if($son_parent['id_menu'] == $permiso['id_menu']){
                                $validate_son = true;
                            }
                        }
                        if($validate_son == true){
                            $children_menu[] = array(
                                'id_menu' => $son_parent['id_menu'],
                                'etiqueta' => $son_parent['etiqueta'],
                                'url' => $son_parent['url'],
                            );
                        }
                    }
                    $menu[] = array(
                        'id_permiso_perfil' => $permiso['id_permiso_perfil'],
                        'id_menu' => $permiso['id_menu'],
                        'id_perfil' => $permiso['id_perfil'],
                        'etiqueta' => $parent['etiqueta'],
                        'fa_icon' => $parent['fa_icon'],
                        'children_menu' => $children_menu,
                    );
                }
            }

            $data = [
                'id_usuario_session' => $_SESSION['logged_in']['id_usuario'],
                'username_session' => $_SESSION['logged_in']['username'],
                'nombre_session' => $_SESSION['logged_in']['nombre'],
                'ape_paterno_session' => $_SESSION['logged_in']['ape_paterno'],
                'ape_materno_session' => $_SESSION['logged_in']['ape_materno'],
                'foto_perfil_session' => $_SESSION['logged_in']['foto_perfil'],
                'id_perfil_session' => $_SESSION['logged_in']['id_perfil'],
                'letter_session' => strtoupper(substr($_SESSION['logged_in']['nombre'], 0, 1)),
                'menu' => $menu,
                'base_url' => base_url(),
            ];
            return $data;
        }
        else
        {
            header('Location: '.base_url());
            exit;
        }
    }

	public function get_perfiles($array, $id){
        $data = array();
        foreach($array as $object){
            $data[] = array(
                'id_perfil' => $object['id_perfil'],
                'perfil_descripcion' => $object['perfil_descripcion'],
                'selected' => ($id == $object['id_perfil']) ? 'selected' : '',
            );
        }
        return $data;
    }

    public function generateRandomString($length = 16) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
