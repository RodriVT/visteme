<div class="col-12 p-0">
    <div class="col-12 pl-4 pt-4 pr-4 pb-2 d-flex title-list">
        <h5 class="text-app col-11 p-0"><?= $title ?></h5>
        <a href="<?= $action ?>" class="col-1 p-1 text-center text-app" title="Nuevo"><i class="fas fa-plus"></i></a>
    </div>
    <div class="col-12 w-100 mt-4 scroll table-responsive">
        <table id="list_opciones_menu" class="table table-primary table-striped table-bordered w-100">
            <thead class="thead-app">
                <tr>
                    <th>Clave</th>
                    <th>Etiqueta</th>
                    <th>Url</th>
                    <th>Clave Padre</th>
                    <th>Icono</th>
                    <th>Orden</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($opciones_menu_data as $opcion_menu){ ?>
                    <tr>
                        <td style="text-align:center"><?= $opcion_menu['id_menu'] ?></td>
                        <td><?= $opcion_menu['etiqueta'] ?></td>
                        <td><?= $opcion_menu['url'] ?></td>
                        <td><?= $opcion_menu['parent_menu_id'] ?></td>
                        <td><?= $opcion_menu['fa_icon'] ?></td>
                        <td><?= $opcion_menu['orden'] ?></td>
                        <td style="text-align:center">
                            <?php foreach($opcion_menu['acciones'] as $accion){ ?>
                                <a href="<?= $accion['href'] ?>" class="btn <?= $accion['class'] ?> text-white" title="<?= $accion['title'] ?>"><li class="<?= $accion['icon'] ?>"></li></a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function() {
        //$('#list_options_menu').DataTable();
    });
</script>