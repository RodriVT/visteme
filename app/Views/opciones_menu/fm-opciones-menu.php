<div class="col-12 p-0">
    <div class="col-12 pl-4 pt-4 pr-4 d-flex title-list">
        <h5 class="text-app col-11 p-0"><?= $title ?></h5>
    </div>
    <div class="col-12 w-100 mt-4">
        <form class="form-horizontal form-label-left input_mask row input-design label-design select-design" action="<?= $action ?>" method="post" enctype="multipart/form-data">
            <div class="form-group col-12 col-sm-6 col-md-4">
                <label for="varchar">Etiqueta</label>
                <input type="text" class="form-control" name="etiqueta" id="etiqueta" placeholder="Etiqueta" value="<?= $etiqueta ?>" required/>
            </div>
            <div class="form-group col-12 col-sm-6 col-md-4">
                <label for="varchar">Clave Padre</label>
                <select type="text" name="parent_menu_id" id="parent_menu_id" class="form-control" value="<?= $parent_menu_id ?>" required>
                    <option value="0">Seleccione</option>
                    <?php foreach($list_parents_menu as $parent){ ?>
                        <option value="<?= $parent['id_menu'] ?>" <?= $parent['selected_parent'] ?>><?= $parent['etiqueta'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group col-12 col-sm-6 col-md-4">
                <label for="varchar">Icono</label>
                <input type="text" class="form-control" name="fa_icon" id="fa_icon" placeholder="Icono" value="<?= $fa_icon ?>" <?= $disabled_fa_icon ?>/>
            </div>
            <div class="form-group col-12 col-sm-6 col-md-4">
                <label for="varchar">Enlace</label>
                <input type="text" class="form-control" name="url" id="url" placeholder="Enlace" value="<?= $url ?>" <?= $disabled_url ?>/>
            </div>
            <div class="form-group col-12 col-sm-6 col-md-4">
                <label for="varchar">Orden</label>
                <input type="text" class="form-control" name="orden" id="orden" placeholder="Orden" value="<?= $orden ?>" required/>
            </div>
            <div class="form-group col-12 d-flex">
                <input type="hidden" name="id_menu" id="id_menu" value="<?= $id_menu ?>" />
        	    <button id="crear" class="btn btn-primary col-5 col-sm-3 col-md-3 col-lg-2 col-xl-1 offset-1 offset-sm-3 offset-md-6 offset-lg-8 offset-xl-9 me-2"><?= $button ?></button>
        	    <a href="javascript: history.go(-1)" class="btn btn-outline-dark col-5 col-sm-3 col-md-3 col-lg-2 col-xl-1 ms-2">Cancelar</a>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#parent_menu_id").change(function(){
            console.log();
            if($(this).val() == 0){
                $("#fa_icon").prop("disabled", false);
                $("#url").prop("disabled", true);
                $("#url").val('');
            }else{
                $("#fa_icon").val('');
                $("#fa_icon").prop("disabled", true);
                $("#url").prop("disabled", false);
            }
        });
    });
</script>