<div class="col-12 p-0">
    <div class="col-12 pl-4 pt-4 pr-4 pb-2 d-flex title-list">
        <h5 class="text-app col-11 p-0"><?= $title ?></h5>
        <a href="<?= $action ?>" class="col-1 p-1 text-center text-app" title="Nuevo"><i class="fas fa-plus"></i></a>
    </div>
    <div class="col-12 w-100 mt-4 mb-5 scroll">
        <table id="list_usuarios" class="table table-primary table-striped table-bordered table-responsive w-100">
            <thead class="thead-app">
                <tr>
                    <th>Clave</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Perfil</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($usuarios_data as $usuario){ ?>
                    <tr>
                        <td style="text-align:center"><?= $usuario['id_usuario'] ?></td>
                        <td><?= $usuario['nombre'].' '.$usuario['ape_paterno'].' '.$usuario['ape_materno'] ?></td>
                        <td><?= $usuario['email'] ?></td>
                        <td><?= $usuario['perfil_descripcion'] ?></td>
                        <td style="text-align:center">
                            <?php foreach($usuario['acciones'] as $accion){ ?>
                                <a href="<?= $accion['href'] ?>" class="btn <?= $accion['class'] ?> text-white" title="<?= $accion['title'] ?>" data-user="<?= $accion['data'] ?>"><li class="<?= $accion['icon'] ?>"></li></a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function() {
        //$('#list_usuarios').DataTable();
        $(document).on('click', '.restore-password', function(){
            $.blockUI({
                message: '<img src="<?= $base_url ?>/assets/img/loader.gif" width="200px" height="100%"/>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: 'rgba(0,0,0,0)',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    color: '#000',
                    width: '300px',
                    top:  ($(window).height() - 300) /2 + 'px',
                    left: ($(window).width() - 300) /2 + 'px',
                }
            });
            var user = $(this).data('user');
            $.post("<?= $base_url ?>/usuarios/get_user",{ id_user: user }, function(data, status){
                var data_user = JSON.parse(data); 
                if(data_user['id_role'] == 2){
                    $.post("<?= $base_url ?>/mailing/send_password_company.php",{ email:data_user['email'] , token:data_user['token'] }, function(data, status){
                        $.unblockUI();
                        swal({
                            title: "\u00A1Enviado!",
                            text: "Se envi\u00F3 un mail a la cuenta de correo eletr\u00F3nico del usuario.",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                        });
                    });  
                }else{
                    $.post("<?= $base_url ?>/mailing/send_password.php",{ email:data_user['email'] , token:data_user['token'] }, function(data, status){
                        $.unblockUI();
                        swal({
                            title: "\u00A1Enviado!",
                            text: "Se envi\u00F3 un mail a la cuenta de correo eletr\u00F3nico del usuario.",
                            type: "success",
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                        });
                    });     
                }
            });
        });
    });
</script>