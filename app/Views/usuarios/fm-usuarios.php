<div class="col-12 p-0 delete-float">
    <div class="col-12 pl-4 pt-4 pr-4 pb-2 d-flex title-list">
        <h5 class="text-app col-11 p-0"><?= $title ?></h5>
    </div>
    <div class="col-12 w-100 mt-4 mb-5">
        <form id="form_users" class="form-horizontal form-label-left input_mask row input-design label-design select-design" action="<?= $action ?>" method="post" enctype="multipart/form-data">
            <div class="form-group col-12 col-sm-6 col-md-4 col-xl-3">
                <label for="varchar">Username</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?= $username ?>" required/>
            </div>
            <div class="form-group col-12 col-sm-6 col-md-4 col-xl-3 <?= $disabled_password ?>">
                <label for="varchar">Contrase&ntilde;a</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Contrase&ntilde;a" <?= $required_password ?>/>
            </div>
            <div class="form-group col-12 col-sm-6 col-md-4 col-xl-3">
                <label for="varchar">Nombre</label>
                <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" value="<?= $nombre ?>" required/>
            </div>
            <div class="form-group col-12 col-sm-6 col-md-4 col-xl-3">
                <label for="varchar">Apellido Paterno</label>
                <input type="text" class="form-control" name="ape_paterno" id="ape_paterno" placeholder="Apellido Paterno" value="<?= $ape_paterno ?>" required/>
            </div>
            <div class="form-group col-12 col-sm-6 col-md-4 col-xl-3">
                <label for="varchar">Apellido Materno</label>
                <input type="text" class="form-control" name="ape_materno" id="ape_materno" placeholder="Apellido Materno" value="<?= $ape_materno ?>" required/>
            </div>
            <div class="form-group col-12 col-sm-6 col-md-4 col-xl-3">
                <label for="varchar">Correo Electr&oacute;nico</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Correo Electr&oacute;nico" value="<?= $email ?>" required/>
            </div>
            <div class="form-group col-12 col-sm-6 col-md-4 col-xl-3">
                <label for="varchar">Tel&eacute;fono</label>
                <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Tel&eacute;fono" value="<?= $telefono ?>" required/>
            </div>
            <div class="form-group col-12 col-sm-6 col-md-4 col-xl-3">
                <label for="varchar">Perfil</label>
                <select type="text" name="id_perfil" id="id_perfil" class="form-control" value="<?= $id_perfil_v ?>" required>
                    <option value="0">Seleccione</option>
                    <?php foreach($perfiles as $perfil){ ?>
                        <option value="<?= $perfil['id_perfil'] ?>" <?= $perfil['selected'] ?>><?= $perfil['perfil_descripcion'] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group col-sm-12 col-md-6 p-0 row m-0 mt-5">
                <div class="col-sm-12 col-md-5">
                    <div id="content_image_profile">
                        <?php if($foto_perfil != ''){ ?>
                            <img src='<?= $base_url ?>/assets/img/img_users/<?= $id_usuario ?>/<?= $foto_perfil ?>' />
                            <div class="toils-user">
                                <i class="fas fa-times-circle delete-image"></i>
                                <i class="fas fa-search-plus img-preview" data-image="<?= $base_url ?>/assets/img/img_users/<?= $id_usuario ?>/<?= $foto_perfil ?>"></i>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-sm-12 col-md-7 buttons-user">
                    <div id="select_images"><i class="fas fa-image"></i> Seleccionar Im&aacute;gen</div>
                    <input class="d-none" type="file" id="file" name="file">
                    <div id="screen_cam" class="mt-3"><i class="fas fa-camera"></i> Tomar Captura</div>
                </div>
            </div>
            <div class="col-12 buttons-forms d-flex">
                <input value="" id="img_cam" name="img_cam" class="d-none">
                <input type="text" class="form-control d-none" name="foto_perfil_new" id="foto_perfil_new" value="<?= $foto_perfil ?>"/>
                <input type="text" class="form-control d-none" name="foto_perfil" id="foto_perfil" value="<?= $foto_perfil ?>"/>
                <input type="text" class="form-control d-none" name="foto_perfil_old" id="foto_perfil_old" value="<?= $foto_perfil_old ?>"/>
                <input type="text" class="form-control d-none" name="id_usuario" id="id_usuario" value="<?= $id_usuario ?>"/>
                <button id="crear" class="btn btn-primary col-5 col-sm-3 col-md-3 col-lg-2 col-xl-1 offset-1 offset-sm-3 offset-md-6 offset-lg-8 offset-xl-9 me-2"><?= $button ?></button>
        	    <a href="javascript: history.go(-1)" class="btn btn-outline-dark col-5 col-sm-3 col-md-3 col-lg-2 col-xl-1 ms-2">Cancelar</a>
            </div>
        </form>
    </div>
</div>
<div id="preview_image" class="modal fade align-middle" role="dialog">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div id="image_content" class="modal-content">
        </div>
    </div>
</div>
<div id="cam" class="modal fade align-middle" role="dialog">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-cam">
        <div id="image_content" class="modal-content col-12 modal-content-cam row m-0">

            <div class="col-12 col-md-6 text-center p-4">
                <video id="video"></video>
            </div>
            <div class="col-12 col-md-6 text-center p-4">
                <canvas id="canvas"></canvas>
            </div>
            <div class="col-12 text-center mb-4">
                <button id="btn_screen" class="btn btn-primary">Toma una foto</button>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load',init);
    function init(){
        var video = document.querySelector('#video'), canvas = document.querySelector('#canvas'), btn = document.querySelector('#btn_screen'), img = document.querySelector('#img_cam');
        navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
        if(navigator.getUserMedia){
            navigator.getUserMedia(
                {video:true},
                function(stream){
                    video.srcObject = stream;
                    video.play();
                },
                function(e){
                    console.log(e);
                }
            )
        }else{
            alert('Tienes un navegador obsoleto');
        }

        video.addEventListener('loadedmetadata',function(){
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
        },false);

        btn.addEventListener('click',function(){
            canvas.getContext('2d').drawImage(video,0,0);
            var imgData = canvas.toDataURL("image/png");
            img.setAttribute('value',imgData);
            $("#content_image_profile").prepend('<img src="'+imgData+'" /><div class="toils-user"><i class="fas fa-times-circle delete-image"></i><i class="fas fa-search-plus img-preview" data-image="'+imgData+'"></i></div>');
        });
    }
    var validate_email = true;
    $(document).ready(function(){
        $("#form_users").submit(function(e){
            console.log();
            if(validate_email == false){
                e.preventDefault();
            }
        });
        $("#email").focusout(function(e){
            var verify_email = IsEmail($("#email").val());
            if(verify_email == true){
                $.post("<?= $base_url ?>/usuarios/validate_email",{ email: $("#email").val() }, function(data, status){
                    user = JSON.parse(data);
                    console.log();
                    if(user == null){
                        $("#email").removeClass('is-invalid');
                        validate_email = true;
                    }else{
                        validate_email = false;
                        $("#email").addClass('is-invalid');
                        if(e.originalEvent.relatedTarget != null){
                            console.log();
                            if(e.originalEvent.relatedTarget.id != 'btn_action'){
                                alertify.error('El correo que ingres\u00F3, ya esta siendo utilizado.');
                            }
                        }else{
                            alertify.error('El correo que ingres\u00F3, ya esta siendo utilizado.');
                        }
                    }
                });
            }else{
                $("#email").addClass('is-invalid');
                alertify.error('El correo que ingres\u00F3, ya esta siendo utilizado.');
            }
        });
        $("#select_images").click(function(){
            $("#file").click();
        });
        $("#file").on("change", function(){
            var image_verify = true;
            var image = $("#file").prop("files")[0];
            var navegador = window.URL || window.webkitURL;
            var certify = validarExtension(image);
            if(certify == true){
                $('#foto_perfil').val('');
                $("#content_image_profile").html('');
                $("#foto_perfil_new").val($("#file").prop("files")[0]['name']);
                var objeto_url = navegador.createObjectURL(image);
                $("#content_image_profile").prepend('<img src="'+objeto_url+'" /><div class="toils-user"><i class="fas fa-times-circle delete-image"></i><i class="fas fa-search-plus img-preview" data-image="'+objeto_url+'"></i></div>');
            }else{
                image_verify = false;
            }
            if(image_verify == false){
                swal('Algunos archivos no estan dentro de los formatos permitidos (.png, .gif, .jpeg, .jpg, .bmp, .tiff, .tif, .jfif).');
            }
        });
        $(document).on('click', '.delete-image', function () {
            $("#content_image_profile").html('');
            $('#foto_perfil').val('');
            $("#foto_perfil_new").val('');
            $("#img_cam").val('');
            var canvas = document.getElementById("canvas");
            var ctx = canvas.getContext("2d");
            ctx.clearRect(0, 0, canvas.width, canvas.height);
        });
        $(document).on('click', '.img-preview', function () {
            var image = $(this).data('image');
            $("#image_content").html('');
            $("#image_content").append('<img src="'+image+'" width="100%" height="auto">');
            $(function(){
                $("#preview_image").appendTo("body");
            });
            $("#preview_image").modal('show');
        });
        $(document).on('click', '#screen_cam', function () {
            $(function(){
                $("#cam").appendTo("body");
            });
            $("#cam").modal('show');
        });
    });
</script>