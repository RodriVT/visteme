<div class="col-12 p-0">
    <div class="col-12 pl-4 pt-4 pr-4 pb-2 d-flex title-list">
        <h5 class="text-app col-11 p-0"><?= $title ?></h5>
        <a href="<?= $action ?>" class="col-1 p-1 text-center text-app" title="Nuevo"><i class="fas fa-plus"></i></a>
    </div>
    <div class="col-12 w-100 mt-4 mb-5 scroll">
        <table id="list_formas_pago" class="table table-primary table-striped table-bordered table-responsive w-100">
            <thead class="thead-app">
                <tr>
                    <th>Clave</th>
                    <th>Descripci&oacute;n</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($formas_pago_data as $forma_pago){ ?>
                    <tr>
                        <td style="text-align:center"><?= $forma_pago['id_forma_pago'] ?></td>
                        <td><?= $forma_pago['forma_pago_descripcion'] ?></td>
                        <td style="text-align:center">
                            <?php foreach($forma_pago['acciones'] as $accion){ ?>
                                <a href="<?= $accion['href'] ?>" class="btn <?= $accion['class'] ?> text-white" title="<?= $accion['title'] ?>"><li class="<?= $accion['icon'] ?>"></li></a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>