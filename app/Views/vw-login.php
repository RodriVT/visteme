<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="assets/css/bootstrap-5.0.0.min.css" rel="stylesheet">
        <link href="assets/css/sweetalert-2.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@300;600&display=swap" rel="stylesheet">
        <link href="assets/fonts/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/fonts/css/brands.min.css" rel="stylesheet">
        <link href="assets/fonts/css/solid.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/style.css">
        <script src="assets/js/jquery-3.5.1.min.js"></script>
        <title>Login - Visteme</title>
    </head>
    <body id="login" class="bg-dark">
        <section>
            <div class="row g-0">
                <div class="col-lg-7 d-none d-lg-block">
                    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                        <ol class="carousel-indicators">
                        <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"></li>
                        <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"></li>
                        <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item img-1 min-vh-100 active">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Da la mejor impresi&oacute;n de ti</h5>
                                    <a href="#" class="text-muted text-decoration-none"></a>
                                </div>
                            </div>
                            <div class="carousel-item img-2 min-vh-100">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Viste a la moda</h5>
                                    <a href="#" class="text-muted text-decoration-none"></a>
                                </div>
                            </div>
                            <div class="carousel-item img-3 min-vh-100">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>Sin miedo al &eacute;xito</h5>
                                    <a href="#" class="text-muted text-decoration-none"></a>
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 d-flex flex-column align-items-end min-vh-100">
                    <div class="px-lg-5 pt-lg-4 pb-lg-3 pt-4 w-100 mb-auto">
                        <div class="area-center col-4">
                            <h3>VIST<span>EME</span></h3>
                        </div>
                    </div>
                    <div class="px-lg-5 py-lg-4 p-4 w-100 align-self-center">
                        <h1 class="font-weight-bold mb-4">Inicia Sesi&oacute;n</h1>
                        <div class="mb-5">
                            <div class="mb-4">
                                <label for="exampleInputEmail1" class="form-label font-weight-bold">Username</label>
                                <input type="text" class="form-control bg-dark-x border-0" placeholder="Ingresa tu usuario" id="username" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-4">
                                <label for="exampleInputPassword1" class="form-label font-weight-bold">Password</label>
                                <input type="password" class="form-control bg-dark-x border-0 mb-2" placeholder="Ingresa tu password" id="password">
                                <!--<a href="#" class="form-text text-muted text-decoration-none">¿Haz olvidado tu password?</a>-->
                            </div>
                            <button type="submit" id="btn_entrar" class="btn btn-primary w-100">Entrar</button>
                        </div>
                    </div>
                    <div class="text-center px-ñg-5 pt-lg-3pb-lg-4 p-4 w-100 mt-auto">
                        <!--<p class="d-inline-block mb-0">¿Todav&iacute;a no tienes una cuenta? </p><a href="#" class="text-light font-weight-bold text-decoration-none"> Crea una ahora</a>-->
                    </div>
                </div>
            </div>
        </section>
        <!-- Optional JavaScript; choose one of the two! -->
        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="assets/js/bootstrap-5.0.0.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/sweetalert-2.min.js"></script>
        <script src="assets/js/functions-generals.js"></script>
        <script>
            $("#btn_entrar").click(function() {
                if($("#username").val() == "" && $("#password").val() == ""){
                    alertMessage( "El usuario/password estan vacíos.", "error");
                }else{
                    if($("#username").val() == ""){
                        alertMessage( "El usuario va vacío.", "error");
                    } else {
                        if($("#password").val() == ""){
                            alertMessage( "El password va vacío.", "error");
                        } else{
                            login($("#username").val(), $("#password").val());
                        }
                    }
                }
            });
            function login(username, password){
                $.post("<?= base_url() ?>/verifylogin/login",{ username: username, password: password }, function(data, status){
                    console.log();
                    if(data == '200'){
                        window.location = "home";
                    }else if(data == '400'){
                        alertMessage( "El usuario/password son incorrectos.", "error");
                    }else if(data == '401'){
                        alertMessage( "La cuenta ha sido bloqueada.", "error");
                    }
                });
            }
        </script>
    </body>
</html>