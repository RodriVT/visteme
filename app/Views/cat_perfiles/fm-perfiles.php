<div class="col-12 p-0">
    <div class="col-12 pl-4 pt-4 pr-4 pb-2 d-flex title-list">
        <h5 class="text-app col-11 p-0"><?= $title ?></h5>
    </div>
    <div class="col-12 w-100 mt-4 mb-5">
        <form class="form-horizontal form-label-left input_mask row input-design label-design select-design" action="<?= $action ?>" method="post" enctype="multipart/form-data">
            <div class="form-group col-12">
                <label for="varchar">Descripci&oacute;n</label>
                <input type="text" class="form-control" name="perfil_descripcion" id="perfil_descripcion" placeholder="Descripci&oacute;n" value="<?= $perfil_descripcion ?>" required/>
            </div>
            <div class="form-group col-12 mt-4 d-flex">
        	    <input type="hidden" name="id_perfil" value="<?= $id_perfil ?>" />
        	    <button id="crear" class="btn btn-primary col-5 col-sm-3 col-md-3 col-lg-2 col-xl-1 offset-1 offset-sm-3 offset-md-6 offset-lg-8 offset-xl-9 me-2"><?= $button ?></button>
        	    <a href="javascript: history.go(-1)" class="btn btn-outline-dark col-5 col-sm-3 col-md-3 col-lg-2 col-xl-1 ms-2">Cancelar</a>
            </div>
        </form>
    </div>
</div>