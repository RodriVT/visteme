<div class="col-12 p-0">
    <div class="col-12 pl-4 pt-4 pr-4 pb-2 d-flex title-list">
        <h5 class="text-app col-11 p-0"><?= $title ?></h5>
    </div>
    <div class="col-12 w-100 mt-4 mb-5">
        <form class="form-horizontal form-label-left input_mask row input-design label-design select-design" action="<?= $action ?>" method="post" enctype="multipart/form-data">
            <div class="form-group col-6">
                <label for="varchar">Descripci&oacute;n</label>
                <input type="text" class="form-control" name="subcategoria_descripcion" id="subcategoria_descripcion" placeholder="Descripci&oacute;n" value="<?= $subcategoria_descripcion ?>" required/>
            </div>
            <div class="form-group col-6">
                <label for="varchar">Categor&iacute;a</label>
                <select name="id_categoria" class="form-control" id="id_categoria" value="<?= $id_categoria ?>" required>
                    <option value="0">--- Seleccione ---</option>
                    <?php
                        foreach ($categorias as $categoria) {
                            if($categoria['id_categoria'] == $id_categoria){
                    ?>
                        <option selected value="<?= $categoria['id_categoria'] ?>"><?= $categoria['categoria_descripcion'] ?></option>
                    <?php } else { ?>
                        <option value="<?= $categoria['id_categoria'] ?>"><?= $categoria['categoria_descripcion'] ?></option>
                    <?php }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group col-12 mt-4">
        	    <input type="hidden" name="id_subcategoria" value="<?= $id_subcategoria ?>" />
        	    <button id="crear" class="btn btn-primary btn-app col-4 col-md-1 offset-2 offset-md-9 mr-2"><?= $button ?></button>
        	    <a href="javascript: history.go(-1)" class="btn btn-cancel col-4 col-md-1 ml-2">Cancelar</a>
            </div>
        </form>
    </div>
</div>