<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Visteme</title>
        <link rel="stylesheet" href="<?= base_url() ?>/assets/css/style.css">
        <link href="<?= base_url() ?>/assets/css/bootstrap-5.0.0.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>/assets/css/sweetalert-2.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>/assets/fonts/css/fontawesome.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>/assets/fonts/css/brands.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>/assets/fonts/css/solid.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>/assets/fonts/css/sweetalert-2.min.css" rel="stylesheet">


        <script src="<?= base_url() ?>/assets/js/jquery-3.5.1.min.js"></script>
        <script src="<?= base_url() ?>/assets/js/sweetalert-2.min.js"></script>
        <script src="<?= base_url() ?>/assets/js/functions-generals.js"></script>
        <script src="<?= base_url() ?>/assets/js/UIBlock.js"></script>
        <script src="<?= base_url() ?>/assets/js/sweetalert-2.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    </head>
    <body>
        <input type="checkbox" id="check">
        <!--header area start-->
        <header class="row">
            <div class="area-left col-1">
                <label for="check">
                    <i class="fas fa-bars" id="sidebar_btn"></i>
                </label>
            </div>
            <div class="area-center col-4">
                <h3>VIST<span>EME</span></h3>
            </div>
            <div class="area-right col-7">
                <i class="fas fa-power-off float-end logout-icon" onclick="logout()"></i>
            </div>
        </header>
        <!--header area end-->
        <!--mobile navigation bar start-->
        <div class="sidebar-movil" data-active="false">
            <div class="profile_info">
                <?php if($session['foto_perfil_session'] != ""){ ?>
                    <img src="<?= base_url()?>/assets/img/img_users/<?= $session['id_usuario_session'] ?>/<?= $session['foto_perfil_session'] ?>" class="profile_image" alt="">
                <?php } else { ?>
                    <img src="<?= base_url()?>/assets/img/img_users/yo.png" class="profile_image" alt="">
                <?php } ?>
                <h4><?= $session['nombre_session'] ?></h4>
            </div>
            <?php if($session['menu'] != null){
                foreach ($session['menu'] as $menu){ ?>
                    <div class="parent style-menu">
                        <div class="active-parent">
                            <i class="<?= $menu['fa_icon'] ?>"></i><span> <?= $menu['etiqueta'] ?></span>
                        </div>
                        <?php if($menu['children_menu'] != null){ ?>
                            <div class="submenu">
                                <?php foreach ($menu['children_menu'] as $som_menu){ ?>
                                    <div class="children">
                                        <div class="active-children">
                                            <a href="<?= base_url()?>/<?= $som_menu['url'] ?>"><i class="far fa-dot-circle"></i><span> <?= $som_menu['etiqueta'] ?></span></a>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
            <?php }
            } ?>
        </div>
        <!--mobile navigation bar end-->
        <!--sidebar start-->
        <div class="sidebar">
            <div class="profile_info">
                <?php if($session['foto_perfil_session'] != ""){ ?>
                    <img src="<?= base_url()?>/assets/img/img_users/<?= $session['id_usuario_session'] ?>/<?= $session['foto_perfil_session'] ?>" class="profile_image" alt="">
                <?php } else { ?>
                    <img src="<?= base_url()?>/assets/img/img_users/yo.png" class="profile_image" alt="">
                <?php } ?>
                <h4><?= $session['nombre_session'] ?></h4>
            </div>
            <?php if($session['menu'] != null){
                foreach ($session['menu'] as $menu){ ?>
                    <div class="parent style-menu">
                        <div class="active-parent">
                            <i class="<?= $menu['fa_icon'] ?>"></i><span> <?= $menu['etiqueta'] ?></span>
                        </div>
                        <?php if($menu['children_menu'] != null){ ?>
                            <div class="submenu">
                                <?php foreach ($menu['children_menu'] as $som_menu){ ?>
                                    <div class="children">
                                        <div class="active-children">
                                            <a href="<?= base_url()?>/<?= $som_menu['url'] ?>"><i class="far fa-dot-circle"></i><span> <?= $som_menu['etiqueta'] ?></span></a>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
            <?php }
            } ?>
        </div>
        <!--sidebar end-->

        <div class="content">