<div class="col-12 p-0">
    <div class="col-12 pl-4 pt-4 pr-4 pb-2 d-flex title-list">
        <h5 class="text-app col-11 p-0"><?= $title ?></h5>
    </div>
    <div class="col-12 w-100 mt-4 mb-5">
        <div class="form-horizontal form-label-left input_mask row input-design label-design select-design">
            <div class="form-group col-3">
                <label for="varchar">Descripci&oacute;n</label>
                <input type="text" class="form-control" name="nombre_producto" id="nombre_producto" placeholder="Descripci&oacute;n" value="<?= $nombre_producto ?>" required/>
            </div>
            <div class="form-group col-3">
                <label for="varchar">SKU</label>
                <input type="text" class="form-control" name="sku" id="sku" placeholder="SKU" value="<?= $sku ?>" required/>
            </div>
            <div class="form-group col-3">
                <label for="varchar">C&oacute;digo</label>
                <input type="text" class="form-control" name="code" id="code" placeholder="C&oacute;digo" value="<?= $code ?>" required/>
            </div>
            <div class="form-group col-3">
                <label for="varchar">Precio</label>
                <input type="number" class="form-control" name="precio" id="precio" placeholder="Precio" value="<?= $precio ?>" required/>
            </div>
            <div class="form-group col-3">
                <label for="varchar">Cantidad</label>
                <input type="text" class="form-control" name="cantidad" id="cantidad" placeholder="Cantidad" value="<?= $cantidad ?>" required/>
            </div>
            <div class="form-group col-3">
                <label for="varchar">Marca</label>
                <select name="id_marca" class="form-control" id="id_marca" value="<?= $id_marca ?>" required>
                    <option value="0">--- Seleccione ---</option>
                    <?php
                        foreach ($marcas as $marca) {
                            if($marca['id_marca'] == $id_marca){
                    ?>
                        <option selected value="<?= $marca['id_marca'] ?>"><?= $marca['marca_descripcion'] ?></option>
                    <?php } else { ?>
                        <option value="<?= $marca['id_marca'] ?>"><?= $marca['marca_descripcion'] ?></option>
                    <?php }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group col-3">
                <label for="varchar">Departamento</label>
                <select name="id_departamento" class="form-control" id="id_departamento" value="<?= $id_departamento ?>" required>
                    <option value="0">--- Seleccione ---</option>
                    <?php
                        foreach ($departamentos as $departamento) {
                            if($departamento['id_departamento'] == $id_departamento){
                    ?>
                        <option selected value="<?= $departamento['id_departamento'] ?>"><?= $departamento['departamento_descripcion'] ?></option>
                    <?php } else { ?>
                        <option value="<?= $departamento['id_departamento'] ?>"><?= $departamento['departamento_descripcion'] ?></option>
                    <?php }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group col-3">
                <label for="varchar">Subcategor&iacute;a</label>
                <select name="id_subcategoria" class="form-control" id="id_subcategoria" value="<?= $id_subcategoria ?>" required>
                    <option value="0">--- Seleccione ---</option>
                    <?php
                        foreach ($subcategorias as $subcategoria) {
                            if($subcategoria['id_subcategoria'] == $id_subcategoria){
                    ?>
                        <option selected value="<?= $subcategoria['id_subcategoria'] ?>"><?= $subcategoria['subcategoria_descripcion'] ?></option>
                    <?php } else { ?>
                        <option value="<?= $subcategoria['id_subcategoria'] ?>"><?= $subcategoria['subcategoria_descripcion'] ?></option>
                    <?php }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group col-3">
                <label for="varchar">Material</label>
                <select name="id_material" class="form-control" id="id_material" value="<?= $id_material ?>" required>
                    <option value="0">--- Seleccione ---</option>
                    <?php
                        foreach ($materiales as $material) {
                            if($material['id_material'] == $id_material){
                    ?>
                        <option selected value="<?= $material['id_material'] ?>"><?= $material['material_descripcion'] ?></option>
                    <?php } else { ?>
                        <option value="<?= $material['id_material'] ?>"><?= $material['material_descripcion'] ?></option>
                    <?php }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group col-3">
                <label for="varchar">Ocasi&oacute;n</label>
                <select name="id_ocasion" class="form-control" id="id_ocasion" value="<?= $id_ocasion ?>" required>
                    <option value="0">--- Seleccione ---</option>
                    <?php
                        foreach ($ocasiones as $ocasion) {
                            if($ocasion['id_ocasion'] == $id_ocasion){
                    ?>
                        <option selected value="<?= $ocasion['id_ocasion'] ?>"><?= $ocasion['ocasion_descripcion'] ?></option>
                    <?php } else { ?>
                        <option value="<?= $ocasion['id_ocasion'] ?>"><?= $ocasion['ocasion_descripcion'] ?></option>
                    <?php }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group col-3">
                <label for="varchar">Color</label>
                <select name="id_color" class="form-control" id="id_color" value="<?= $id_color ?>" required>
                    <option value="0">--- Seleccione ---</option>
                    <?php
                        foreach ($colores as $color) {
                            if($color['id_color'] == $id_color){
                    ?>
                        <option selected value="<?= $color['id_color'] ?>"><?= $color['color_descripcion'] ?></option>
                    <?php } else { ?>
                        <option value="<?= $color['id_color'] ?>"><?= $color['color_descripcion'] ?></option>
                    <?php }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group col-3">
                <label for="varchar">Tama&ntilde;o</label>
                <select name="id_tamanio" class="form-control" id="id_tamanio" value="<?= $id_tamanio ?>" required>
                    <option value="0">--- Seleccione ---</option>
                    <?php
                        foreach ($tamanios as $tamanio) {
                            if($tamanio['id_tamanio'] == $id_tamanio){
                    ?>
                        <option selected value="<?= $tamanio['id_tamanio'] ?>"><?= $tamanio['tamanio_descripcion'] ?></option>
                    <?php } else { ?>
                        <option value="<?= $tamanio['id_tamanio'] ?>"><?= $tamanio['tamanio_descripcion'] ?></option>
                    <?php }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group col-3">
                <label for="varchar">Tipo</label>
                <select name="id_tipo" class="form-control" id="id_tipo" value="<?= $id_tipo ?>" required>
                    <option value="0">--- Seleccione ---</option>
                    <?php
                        foreach ($tipos as $tipo) {
                            if($tipo['id_tipo'] == $id_tipo){
                    ?>
                        <option selected value="<?= $tipo['id_tipo'] ?>"><?= $tipo['tipo_descripcion'] ?></option>
                    <?php } else { ?>
                        <option value="<?= $tipo['id_tipo'] ?>"><?= $tipo['tipo_descripcion'] ?></option>
                    <?php }
                        }
                    ?>
                </select>
            </div>
            <div class="form-group col-12">
                <div id="content_images" class="col-12 row m-0 scroll mb-3">
                </div>
                <div id="select_images"><i class="fas fa-image"></i> Seleccionar Im&aacute;genes</div>
                <input class="d-none" type="file" id="file" name="files[]" multiple>
            </div>
            <div class="form-group col-12 mt-4">
        	    <input type="hidden" name="id_producto" id="id_producto" value="<?= $id_producto ?>" />
                <input type="hidden" name="keyprod" id="keyprod" value="<?= $keyprod ?>"/>
                <button id="btn_save" type="submit" class="btn btn-primary btn-app col-4 col-md-1 offset-2 offset-md-9 mr-2"><?= $button ?></button>
        	    <a href="javascript: history.go(-1)" class="btn btn-cancel col-4 col-md-1 ml-2">Cancelar</a>
            </div>
        </div>
    </div>
</div>
<div id="preview_image" class="modal fade align-middle" role="dialog">
    <div class="modal-dialog modal-xl modal-dialog-centered">
        <div id="image_content" class="modal-content">
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var imagenes  = [];
        var first_photo = true;
        var no_photo = 1;
        $("#select_images").click(function(){
            $("#file").click();
        });
        $("#file").on("change", function(){
            var image_verify = true;
            var images = document.getElementById('file').files;
            var navegador = window.URL || window.webkitURL;
            for(var x=0; x<images.length; x++){
                var certify = validarExtension(images[x]);
                if(certify == true){
                    console.log();
                    if(first_photo == true){
                        $("#content_images").html('');
                        first_photo = false;
                    }
                    var objeto_url = navegador.createObjectURL(images[x]);
                    imagenes.push({
                        'id' : no_photo,
                        'image' : images[x],
                    });
                    $("#content_images").prepend('<div id="upload_'+no_photo+'" class="col-4 mt-2 upload"><div class="img-upload"><img src='+objeto_url+' /></div><div class="toils"><i class="fas fa-times-circle delete-image" data-id="'+no_photo+'"></i><i class="fas fa-search-plus img-preview" data-image="'+objeto_url+'"></i></div></div>');
                    no_photo++;
                }else{
                    image_verify = false;
                }
            }
            if(image_verify == false){
                swal('Algunos archivos no estan dentro de los formatos permitidos (.png, .gif, .jpeg, .jpg, .bmp, .tiff, .tif, .jfif).');
            }
        });
        $(document).on('click', '.delete-image', function () {
            var id = $(this).data('id');
            $("#upload_"+id).remove();
            for(var foto in imagenes){
                console.log();
                if(imagenes[foto].id == id){
                    imagenes.splice( foto, 1 );
                }
            }
        });
        $(document).on('click', '.img-preview', function () {
            var image = $(this).data('image');
            $("#image_content").html('');
            $("#image_content").append('<img src="'+image+'" width="100%" height="auto">');
            $(function(){
                $("#preview_image").appendTo("body");
            });
            $("#preview_image").modal('show');
        });
        $("#btn_save").click(function(){
            var form = [];
            var validate_form = true;
            form.push({
                "nombre_producto" : $('#nombre_producto').val() != '' ?  true : false,
                "sku" : $('#sku').val() != '' ?  true : false,
                "code" : $('#code').val() != '' ?  true : false,
                "precio" : $('#precio').val() != '' ?  true : false,
                "cantidad" : $('#cantidad').val() != '' ?  true : false,
                "id_marca" : $('#id_marca').val() != 0 ?  true : false,
                "id_departamento" : $('#id_departamento').val() != 0 ?  true : false,
                "id_subcategoria" : $('#id_subcategoria').val() != 0 ?  true : false,
                "id_material" : $('#id_material').val() != 0 ?  true : false,
                "id_ocasion" : $('#id_ocasion').val() != 0 ?  true : false,
                "id_color" : $('#id_color').val() != 0 ?  true : false,
                "id_tamanio" : $('#id_tamanio').val() != 0 ?  true : false,
                "id_tipo" : $('#id_tipo').val() != 0 ?  true : false
            });
            console.log();
            $.each(form[0], function (ind, elem) {
                if(elem == false){
                    $("#"+ind).addClass('border-invalidate');
                    validate_form = false;
                }else{
                    $("#"+ind).removeClass('border-invalidate');
                }
            });
            if(validate_form == true){
                $.blockUI({
                    message: '<img src="<?= $base_url ?>/assets/img/loader.gif" width="200px" height="100%"/>',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: 'rgba(0,0,0,0)',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        color: '#000',
                        width: '300px',
                        top:  ($(window).height() - 300) /2 + 'px',
                        left: ($(window).width() - 300) /2 + 'px',
                    }
                });
                var xhr = new XMLHttpRequest();
                var formData = new FormData();
                $.each(imagenes, function(key, value){
                    formData.append("imagenes[]",imagenes[key].image);
                });
                formData.append("id_producto",$("#id_producto").val());
                formData.append("nombre_producto",$("#nombre_producto").val());
                formData.append("sku",$("#sku").val());
                formData.append("code",$("#code").val());
                formData.append("precio",$("#precio").val());
                formData.append("cantidad",$("#cantidad").val());
                formData.append("id_marca",$("#id_marca").val());
                formData.append("id_departamento",$("#id_departamento").val());
                formData.append("id_subcategoria",$("#id_subcategoria").val());
                formData.append("id_material",$("#id_material").val());
                formData.append("id_ocasion",$("#id_ocasion").val());
                formData.append("id_color",$("#id_color").val());
                formData.append("id_tamanio",$("#id_tamanio").val());
                formData.append("id_tipo",$("#id_tipo").val());
                xhr.open("POST","create_action");
                xhr.send(formData);
                xhr.abort;
                setTimeout(function() {
                    $.unblockUI();
                    history.back(1);
                }, 7000);
            }
        });
    });

    function validarExtension(datos) {
        var extensionesValidas = ".png, .gif, .jpeg, .jpg, .bmp, .tiff, .tif, .jfif";
        var ruta = datos['name'];
        var extension = ruta.substring(ruta.lastIndexOf('.') + 1).toLowerCase();
        var extensionValida = extensionesValidas.indexOf(extension);

        if(extensionValida < 0) {
            return false;
        } else {
            return true;
        }
    }
</script>