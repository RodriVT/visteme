<div class="col-12 p-0">
    <div class="col-12 pl-4 pt-4 pr-4 pb-2 d-flex title-list">
        <h5 class="text-app col-11 p-0"><?= $title ?></h5>
    </div>
    <div class="col-12 w-100 mt-4 mb-5">
        <h4>Perfil: <?= $perfil['perfil_descripcion'] ?></h4>
        <form class="form-horizontal form-label-left input_mask row input-design label-design select-design w-100 m-0 scroll table-responsive" action="<?= $action ?>" method="post" enctype="multipart/form-data">
            <table id="list_users_permissions" class="table table-primary table-striped table-bordered table-responsive w-100">
                <thead class="thead-app">
                    <tr>
                        <th></th>
                        <th>Principales</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($opciones_menu != null){ ?>
                        <?php foreach($opciones_menu as $parent_menu){ ?>
                            <tr>
                                <td style="text-align:center;"><input type="checkbox" name="<?= $parent_menu['id_menu'] ?>" id="parent_<?= $parent_menu['id_menu'] ?>" class="parents" data-id="<?= $parent_menu['id_menu'] ?>" data-checked="<?= $parent_menu['checked'] ?>" <?= $parent_menu['checked'] ?>></td>
                                <td><?= $parent_menu['etiqueta'] ?></td>
                                <td></td>
                            </tr>
                            <?php if($parent_menu['opciones_children'] != null){ ?>
                                <?php foreach($parent_menu['opciones_children'] as $son_menu){ ?>
                                    <tr>
                                        <td style="text-align:center;"><input type="checkbox" class="check_<?= $son_menu['parent_menu_id'] ?> children" name="<?= $son_menu['id_menu'] ?>" id="son_<?= $son_menu['id_menu'] ?>" data-id="<?= $son_menu['id_menu'] ?>" data-parent="<?= $son_menu['parent_menu_id'] ?>" data-checked="<?= $son_menu['checked'] ?>" <?= $son_menu['checked'] ?>></td>
                                        <td></td>
                                        <td><?= $son_menu['etiqueta'] ?></td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
            <div class="col-12 buttons-forms mt-4 d-flex">
                <input type="text" class="form-control d-none" name="id_perfil" id="id_perfil" value="<?= $perfil['id_perfil'] ?>"/>
                <button id="crear" class="btn btn-primary col-5 col-sm-3 col-md-3 col-lg-2 col-xl-1 offset-1 offset-sm-3 offset-md-6 offset-lg-8 offset-xl-9 me-2"><?= $button ?></button>
        	    <a href="javascript: history.go(-1)" class="btn btn-outline-dark col-5 col-sm-3 col-md-3 col-lg-2 col-xl-1 ms-2">Cancelar</a>
            </div>
        </form>
    </div>
</div>
<script>
    $(".parents").click(function(){
        var id_parent = $(this).data('id');
        if($('#parent_'+id_parent).prop('checked'))
            $('.check_'+id_parent).prop('checked', true);
        else
            $('.check_'+id_parent).prop('checked', false);
    });
    $(".children").click(function(){
        var id_son = $(this).data('id');
        var id_parent = $(this).data('parent');
        if($('#son_'+id_son).prop('checked'))
            $('#parent_'+id_parent).prop('checked', true);
    });
</script>