<div class="col-12 p-0">
    <div class="col-12 pl-4 pt-4 pr-4 pb-2 d-flex title-list">
        <h5 class="text-app col-11 p-0"><?= $title ?></h5>
    </div>
    <div class="col-12 w-100 mt-4 mb-5 scroll">
        <table id="list_permisos" class="table table-primary table-striped table-bordered table-responsive w-100">
            <thead class="thead-app">
                <tr>
                    <th>Clave</th>
                    <th>Perfil</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($perfiles_data as $perfil){ ?>
                    <tr>
                        <td style="text-align:center"><?= $perfil['id_perfil'] ?></td>
                        <td><?= $perfil['perfil_descripcion'] ?></td>
                        <td style="text-align:center">
                            <?php foreach($perfil['acciones'] as $accion){ ?>
                                <a href="<?= $accion['href'] ?>" class="btn <?= $accion['class'] ?> text-white" title="<?= $accion['title'] ?>"><li class="<?= $accion['icon'] ?>"></li></a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function() {
        //$('#list_options_menu').DataTable();
    });
</script>