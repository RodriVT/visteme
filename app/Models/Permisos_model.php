<?php

namespace App\Models;
use CodeIgniter\Model;

class Permisos_model extends Model
{
    protected $table      = 'permisos_perfiles';
    protected $primaryKey = 'id_permiso_perfil';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_perfil', 'id_menu', 'id_estatus', 'creator_user_id', 'updater_user_id'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>