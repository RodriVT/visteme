<?php

namespace App\Models;
use CodeIgniter\Model;

class Materiales_model extends Model
{
    protected $table      = 'cat_materiales';
    protected $primaryKey = 'id_material';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['material_descripcion', 'id_estatus', 'creator_user_id','updater_user_id'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>