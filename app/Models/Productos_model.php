<?php

namespace App\Models;
use CodeIgniter\Model;

class Productos_model extends Model
{
    protected $table      = 'productos';
    protected $primaryKey = 'id_producto';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['keyprod', 'nombre_producto', 'sku', 'code', 'precio', 'id_marca', 'id_departamento', 'id_subcategoria', 'id_material', 'id_ocasion', 'id_tipo', 'id_color', 'id_tamanio', 'cantidad', 'id_estatus', 'creator_user_id','updater_user_id'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>