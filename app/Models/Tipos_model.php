<?php

namespace App\Models;
use CodeIgniter\Model;

class Tipos_model extends Model
{
    protected $table      = 'cat_tipos';
    protected $primaryKey = 'id_tipo';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['tipo_descripcion', 'id_estatus', 'creator_user_id','updater_user_id'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>