<?php

namespace App\Models;
use CodeIgniter\Model;

class Formas_envio_model extends Model
{
    protected $table      = 'cat_formas_envio';
    protected $primaryKey = 'id_forma_envio';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['forma_envio_descripcion', 'id_estatus', 'creator_user_id','updater_user_id'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>