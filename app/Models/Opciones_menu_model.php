<?php

namespace App\Models;
use CodeIgniter\Model;

class Opciones_menu_model extends Model
{
    protected $table      = 'opciones_menu';
    protected $primaryKey = 'id_menu';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['etiqueta', 'url', 'parent_menu_id', 'fa_icon', 'orden', 'id_estatus', 'creator_user_id', 'updater_user_id'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}

?>